/*++

Module Name:

   stdafx.cpp

Abstract:

	The precompiled header: compiled into an intermediate form that 
	is faster to process for the compiler.

--*/

#include "stdafx.h"