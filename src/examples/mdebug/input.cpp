/*++

Module Name:

   input.cpp

Abstract:

	Illustrates how to use the Rift SDK to control the local player
	(or their pet).

--*/

#include "stdafx.h"
#include "input.h"
#include "debug.h"

//
// Whether the window is currently shown in the overlay.
//
bool Input::ShowWindow = false;

char Input::m_TargetPointerAsString[100];
void* Input::m_TargetPointerAsNumber = NULL;

SDKCOLOR Input::m_ColorGreen;
SDKCOLOR Input::m_ColorYellow;

void Input::Initialize(
	void
)
/*++

Routine Description:

	Initializes class specific data structures.

Arguments:

	None.

Return Value:

	None.

--*/
{
	m_TargetPointerAsNumber = _g_LocalPlayer;
	sprintf_s(m_TargetPointerAsString, "%p", m_TargetPointerAsNumber);
	
	//
	// Green.
	//
	m_ColorGreen.BGRA = 0; m_ColorGreen.G = 255; m_ColorGreen.A = 255;

	//
	// Yellow.
	//
	m_ColorYellow.BGRA = 0; m_ColorYellow.R = 255; m_ColorYellow.G = 255; m_ColorGreen.A = 255;

	//
	// Log key presses.
	//
	SdkRegisterOnKeyPress
	(
		[](bool KeyDown, unsigned int VirtualKey, unsigned short Repeated, unsigned char ScanCode, bool IsExtended, bool IsAltDown, bool PreviousState, bool TransitionState, void* UserData) -> void
		{
			UNREFERENCED_PARAMETER(UserData);

			SdkUiConsoleWrite("[mdebug] Key 0x%X (0x%X) [E: %u, A: %u, P: %u, T: %u] was pressed %d times %s.\n", VirtualKey, ScanCode, IsExtended, IsAltDown, PreviousState, TransitionState, Repeated, ((KeyDown) ? "DOWN" : "UP"));
		}, NULL
	);
}

void Input::Draw(
	void
)
/*++

Routine Description:

	This function is invoked each frame while the overlay is present
	and the target window is being shown.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// Get the previous window's location and size.
	//
	SDKPOINT Position, Size;
	SdkUiGetWindowDim(&Position, &Size);

	// 
	// Set this new window to display directly to the right of the 
	// previous window.
	//
	Position.x += (Size.x + 10.f);
	SdkUiSetNextWindowPos(&Position);

	//
	// Create the new window.
	//
	bool Collapsed = false;
	SdkUiBeginWindow("Input", &Input::ShowWindow, &Collapsed);
	if (!Collapsed)
	{
		//
		// Early out here if the window is partially non-visible.
		//
		SdkUiEndWindow();
		return;
	}

	//
	// Elapsed game time (in seconds).
	//
	float Time = 0;
	SdkGetGameTime(&Time);

	SdkUiText("Game time:");
	SdkUiForceOnSameLine();
	SdkUiColoredText(&m_ColorGreen, "%f", Time);
	
	//
	// Ping (in ms).
	//
	unsigned int Ping = 0;
	SdkGetNetPing(&Ping);

	SdkUiText("Ping:");
	SdkUiForceOnSameLine();
	SdkUiColoredText(&m_ColorGreen, "%u", Ping);

	bool OverlayVisible;
	SdkUiIsOverlayVisible(&OverlayVisible);

	bool ChatActive;
	SdkIsChatActive(&ChatActive);

	SdkUiText("Overlay: %u, chat: %u", OverlayVisible, ChatActive);

	SdkUiText("Local player:");
	SdkUiForceOnSameLine();
	SdkUiColoredText(&m_ColorYellow, "0x%p", _g_LocalPlayer);

	static bool BlockGameInput = false;
	bool OnClickBlockGameInput = false;
	SdkUiCheckbox("Block game input", &BlockGameInput, &OnClickBlockGameInput);
	if (OnClickBlockGameInput)
	{
		if (BlockGameInput)
			SdkDisableInput();
		else
			SdkEnableInput();
	}

	static const char* Type[] = { 
		"Stop", 
		"Move", 
		"Attack", 
		"Attack/Move", 
		"Cast Spell", 
		"Level Spell", 
		"Emote", 
		"Send Chat",
		"Buy Item",
		"Sell Item",
		"Mastery Emote"
	};

	//
	// NOTE: This is marked as 'static' so it's effectively a global
	// variable and therefore won't be reset to '0' each frame.
	//
	static int TypeSelection = 0;
	SdkUiCombo("Type", &TypeSelection, Type, RTL_NUMBER_OF(Type), NULL);

	//
	// NOTE: This is marked as 'static' so it's effectively a global
	// variable and therefore won't be reset to 'false' each frame.
	//
	static bool ControlPet = false;

	//
	// Don't render the 'pet' control checkbox beyond the "basic"
	// types as it's not relevant.
	//
	if (TypeSelection < 4)
	{
		SdkUiCheckbox("Control pet", &ControlPet, NULL);
	}

	switch (TypeSelection)
	{
		//
		// Stop.
		//
		case 0:
		{
			bool StopClicked = false;
			SdkUiButton(Type[TypeSelection], &StopClicked);
			if (StopClicked)
				SdkStopLocalPlayer(ControlPet);

			break;
		}
		//
		// Move.
		//
		case 1:
		//
		// Attack/Move.
		//
		case 3:
		{
			//
			// NOTE: This is marked as 'static' so it's effectively a global
			// variable and therefore won't be zeroed each frame.
			//
			static SDKVECTOR Destination;
			SdkUiInputVector("Destination", &Destination, 3, NULL);

			bool ActionClicked = false;
			SdkUiButton(Type[TypeSelection], &ActionClicked);
			if (ActionClicked)
			{
				if (TypeSelection == 1)
					SdkMoveLocalPlayer(&Destination, ControlPet);
				else
					SdkAttackMoveLocalPlayer(&Destination);
			}

			break;
		}
		//
		// Attack.
		//
		case 2:
		//
		// Cast Spell.
		//
		case 4:
		{
			//
			// Should we cast at a target (targeted spell) or a 
			// location (skill shot)?
			//
			static bool ShouldCastAtLocation = false;
			SdkUiCheckbox("Cast at a location", &ShouldCastAtLocation, NULL);

			static SDKVECTOR CastLocation;
			if (ShouldCastAtLocation)
			{
				//
				// The world coordinates (x, y, z) to cast spell at.
				//
				SdkUiInputVector("Cast location", &CastLocation, 3, NULL);
			}
			else 
			{
				SdkUiInputText("Attackable unit", m_TargetPointerAsString, RTL_NUMBER_OF(m_TargetPointerAsString), (TEXT_FLAGS_CHARS_HEXADECIMAL | TEXT_FLAGS_CHARS_UPPERCASE), NULL);
			}
			
			static int SpellSlot = 0;
			if (TypeSelection == 4)
				SdkUiInputNumber("Slot", &SpellSlot, NULL);

			bool ActionClicked = false;
			SdkUiButton(Type[TypeSelection], &ActionClicked);
			if (ActionClicked)
			{
				//
				// Convert entered in object pointer text to a number.
				//
				m_TargetPointerAsNumber = (void*)strtoul(m_TargetPointerAsString, NULL, 16);

				if (TypeSelection == 2)
				{
					//
					// For basic attacks.
					//
					SdkAttackTargetLocalPlayer(m_TargetPointerAsNumber, ControlPet);
				}
				else
				{
					//
					// For spell casting.
					//
					if (ShouldCastAtLocation)
					{
						//
						// Cast the spell at a target location in the world.
						//
						SdkCastSpellLocalPlayer(NULL, &CastLocation, (unsigned char)SpellSlot, SPELL_CAST_START);
					}
					else
					{
						//
						// Cast the spell at a target unit.
						//
						SdkCastSpellLocalPlayer(m_TargetPointerAsNumber, NULL, (unsigned char)SpellSlot, SPELL_CAST_START);
					}
				}
			}

			break;
		}
		//
		// Level Spell.
		//
		case 5:
		//
		// Emote.
		//
		case 6:
		//
		// Buy Item.
		//
		case 8:
		//
		// Sell Item.
		//
		case 9:
		{
			static int Slot = 0;
			SdkUiInputNumber("Slot", &Slot, NULL);

			static int ID = 1001;

			if (TypeSelection == 8)
				SdkUiInputNumber("ID", &ID, NULL);

			bool ActionClicked = false;
			SdkUiButton(Type[TypeSelection], &ActionClicked);
			if (ActionClicked)
			{
				if (TypeSelection == 5)
					SdkLevelSpellLocalPlayer((unsigned char)Slot, NULL);
				else if (TypeSelection == 6)
					SdkShowEmoteLocalPlayer((unsigned char)Slot);
				else if (TypeSelection == 8)
					SdkBuyItemLocalPlayer(ID, Slot, NULL);
				else if (TypeSelection == 9)
					SdkSellItemLocalPlayer(Slot);
			}

			break;
		}
		//
		// Send Chat.
		//
		case 7:
		{	//
			// NOTE: These are marked as 'static' so they're effectively global
			// variables and therefore won't be reset each frame.
			//
			static char MessageContents[1024] = { 0 };
			static SDKPOINT TextboxSize = { -1.0f, 300.f };
			SdkUiInputMultiLineText("Message", MessageContents, RTL_NUMBER_OF(MessageContents), &TextboxSize, 0, NULL);

			static bool LocalOnly = true;
			SdkUiCheckbox("Display locally only", &LocalOnly, NULL);

			bool SendChatClicked = false;
			SdkUiButton(Type[TypeSelection], &SendChatClicked);
			if (SendChatClicked)
			{
				if (LocalOnly)
					SdkDisplayChatLocalPlayer(MessageContents, CHAT_FLAG_WHITE);
				else
					SdkSendChatLocalPlayer(MessageContents);

				memset(MessageContents, 0, sizeof(MessageContents));
			}

			break;
		}
		//
		// Mastery Emote.
		//
		case 10:
		{
			bool ShowMasteryEmoteClicked = false;
			SdkUiButton(Type[TypeSelection], &ShowMasteryEmoteClicked);
			if (ShowMasteryEmoteClicked)
				SdkShowMasteryEmoteLocalPlayer();

			break;
		}
		default:
			break;
	}


	//
	// Signal that we're done displaying this window.
	//
	SdkUiEndWindow();
}