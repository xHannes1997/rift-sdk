/*++

Module Name:

   Drawing.h

Abstract:

	Showcases how to use the Rift SDK to draw lines, circles, and 
	cones atop the game's frame.

--*/

#pragma once

///
/// Classes.
///

class Drawing
{
public:

	//
	// A line object.
	//
	struct Line
	{
		SDKVECTOR Start;
		SDKVECTOR End;
		float Width;
		int Texture;
		SDKCOLOR Color;

		Line(
			void
		) : Width(100.f), Texture(0)
		{
			memset(&Start, 0, sizeof(Start));
			memset(&End, 0, sizeof(End));
			memset(&Color, 0, sizeof(Color));
		}
	};

	//
	// A circle object. 
	//
	struct Circle
	{
		SDKVECTOR Origin;
		float Radius;
		SDKCOLOR Color;
		int Texture;
		SDKVECTOR Direction;

		Circle(
			void
		) : Radius(100.f), Texture(0)
		{
			memset(&Origin, 0, sizeof(Origin));
			memset(&Color, 0, sizeof(Color));
			Direction.x = Direction.y = 0;
			Direction.z = 1;
		}
	};

	//
	// A cone object.
	//
	struct Cone
	{
		SDKVECTOR Origin;
		SDKVECTOR Direction;
		float Length;
		float Angle;
		SDKCOLOR Color;
		int Texture;
		
		Cone(
			void
		) : Length(100.f), Angle(30.f), Texture(0)
		{
			memset(&Origin, 0, sizeof(Origin));
			memset(&Color, 0, sizeof(Color));
			Direction.x = Direction.y = 0;
			Direction.z = 1;
		}
	};

	//
	// A text message.
	//
	struct Text
	{
		SDKVECTOR Position;
		char Message[1000];
		char Face[32];
		SDKCOLOR Color;
		int Height;
		int Width;
		int Weight;
		bool Italic;

		Text(
			void
		): Height(18), Width(0), Weight(0), Italic(false)
		{
			memset(&Position, 0, sizeof(Position));
			memset(Message, 0, sizeof(Message));
			strcpy_s(Face, "Arial");
			memset(&Color, 0, sizeof(Color));
		}
	};

private:
	static Line m_CurrentLine;
	static Circle m_CurrentCircle;
	static Cone m_CurrentCone;
	static Text m_CurrentText;
	static SDKVECTOR m_CurrentColor;

	static std::vector<Line> m_Lines;
	static std::vector<Circle> m_Circles;
	static std::vector<Cone> m_Cones;
	static std::vector<Text> m_Texts;

	static bool m_DrawWaypoint;

public:
	static bool ShowWindow;

	static void Initialize(
		void
	);

	static void DrawOverlay(
		void
	);

	static void DrawGame(
		void
	);

	static void RGBAtoBGRA(
		_In_ SDKVECTOR& Source, 
		_Out_ SDKCOLOR& Destination
	)
	/*++

		Routine Description:

			This function converts an RGB color to a BGRA color.

		Arguments:

			Source - The input RGB color.

			Destination - The output BGRA color.

		Return Value:

			None.

	--*/
	{
		//
		// Convert the RGB color-wheel selection to a game color.
		//

		Destination.R = (uint8_t)(round(Source.x * (UINT8_MAX * 1.f)));
		Destination.G = (uint8_t)(round(Source.y * (UINT8_MAX * 1.f)));
		Destination.B = (uint8_t)(round(Source.z * (UINT8_MAX * 1.f)));
		Destination.A = UINT8_MAX;
	}
};
