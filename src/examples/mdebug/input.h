/*++

Module Name:

   input.h

Abstract:

	Illustrates how to use the Rift SDK to control the local player
	(or their pet).

--*/

#pragma once

///
/// Classes.
///

class Input
{
private:
	static char m_TargetPointerAsString[100];
	static void* m_TargetPointerAsNumber;
	static SDKCOLOR m_ColorGreen;
	static SDKCOLOR m_ColorYellow;

public:
	static bool ShowWindow;

	static void Initialize(
		void
	);

	static void Draw(
		void
	);
};
