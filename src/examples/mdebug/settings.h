/*++

Module Name:

   settings.h

Abstract:

	Illustrates how to use the Rift SDK to store and retrieve persistent
	settings.

--*/

#pragma once

///
/// Classes.
///

class Settings
{
private:
	static char m_Text[100];
	static bool m_Boolean;
	static float m_Float;
	static int m_Number;

public:
	static bool ShowWindow;

	static void Initialize(
		void
	);

	static void Draw(
		void
	);
};