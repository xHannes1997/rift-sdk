/*++

Module Name:

   Drawing.cpp

Abstract:

	Showcases how to use the Rift SDK to draw lines, circles, and
	cones atop the game's frame.

--*/

#include "stdafx.h"
#include "drawing.h"
#include "debug.h"

Drawing::Line Drawing::m_CurrentLine;
Drawing::Circle Drawing::m_CurrentCircle;
Drawing::Cone Drawing::m_CurrentCone;
Drawing::Text Drawing::m_CurrentText;
SDKVECTOR Drawing::m_CurrentColor;

//
// All the line shapes we're drawing per frame.
//
std::vector<Drawing::Line> Drawing::m_Lines;

//
// All the circle shapes we're drawing per frame.
//
std::vector<Drawing::Circle> Drawing::m_Circles;

//
// All the cone shapes we're drawing per frame.
//
std::vector<Drawing::Cone> Drawing::m_Cones;

//
// All the text messages we're drawing per frame.
//
std::vector<Drawing::Text> Drawing::m_Texts;

//
// Whether we should draw the local player's current waypoint.
//
bool Drawing::m_DrawWaypoint = false;

//
// Whether the window is currently shown in the overlay.
//
bool Drawing::ShowWindow = false;

void Drawing::Initialize(
	void
)
/*++

Routine Description:

	Initializes class specific data structures.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// The initial color.
	//
	m_CurrentColor.x = m_CurrentColor.z = 0;
	m_CurrentColor.y = 1;
}

void Drawing::DrawOverlay(
	void
)
/*++

Routine Description:

	This function is invoked each frame while the overlay is present
	and the target window is being shown.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// Get the previous window's location and size.
	//
	SDKPOINT Position, Size;
	SdkUiGetWindowDim(&Position, &Size);

	// 
	// Set this new window to display directly to the right of the 
	// previous window.
	//
	Position.x += (Size.x + 10.f);
	SdkUiSetNextWindowPos(&Position);

	//
	// Create the new window.
	//
	bool Collapsed = false;
	SdkUiBeginWindow("Drawing", &Drawing::ShowWindow, &Collapsed);
	if (!Collapsed)
	{
		//
		// Early out here if the window is partially non-visible.
		//
		SdkUiEndWindow();
		return;
	}

	//
	// RGB color-wheel.
	//
	SdkUiColorPicker("Color", &m_CurrentColor, NULL);

	//
	// Draw waypoint line.
	//
	SdkUiCheckbox("Draw local player waypoint", &m_DrawWaypoint, NULL);

	//
	// The possible shapes that can be drawn.
	//
	static const char* Shapes[] = { "Line", "Circle", "Cone", "Text" };

	//
	// NOTE: This is marked as 'static' so it's effectively a global
	// variable and therefore won't be reset to '0' each frame.
	//
	static int ShapeSelection = 0;
	SdkUiCombo("Shape", &ShapeSelection, Shapes, RTL_NUMBER_OF(Shapes), NULL);

	//
	// Line.
	//
	if (ShapeSelection == 0)
	{
		//
		// Starting position (x, y, z) in world coordinates.
		//
		SdkUiInputVector("Start", &m_CurrentLine.Start, 3, NULL);
		SdkUiForceOnSameLine();
		
		bool StartClicked = false;
		SdkUiSmallButton("Local##1", &StartClicked);
		if (StartClicked)
		{
			//
			// Use local player's position.
			//
			SdkGetObjectPosition(_g_LocalPlayer, &m_CurrentLine.Start);
		}

		//
		// Ending position (x, y, z) in world coordinates.
		//
		SdkUiInputVector("End  ", &m_CurrentLine.End, 3, NULL);
		SdkUiForceOnSameLine();

		bool EndClicked = false;
		SdkUiSmallButton("Local##2", &EndClicked);
		if (EndClicked)
		{
			//
			// Use local player's position.
			//
			SdkGetObjectPosition(_g_LocalPlayer, &m_CurrentLine.End);
		}

		//
		// The width of the line.
		//
		SdkUiInputFloat("Width", &m_CurrentLine.Width, 3, NULL);

		//
		// The style of the line.
		//
		static const char* Textures[] = { "Basic", "Engine - Line 1", "Engine - Line 2", "Engine - Line 3", "Engine - Line 4" };
		SdkUiCombo("Texture", &m_CurrentLine.Texture, Textures, RTL_NUMBER_OF(Textures), NULL);
		if (m_CurrentLine.Texture >= 1)
		{
			SDKCOLOR Color;
			Color.BGRA = 0;
			Color.R = 255;
			Color.A = 255;

			//
			// 'RED' warning text.
			//
			SdkUiColoredText(&Color, "WARNING");
			SdkUiForceOnSameLine();
			SdkUiText("Engine drawing might not display\n"
				"properly for all line colors.");
		}
	}
	//
	// Circle.
	//
	else if (ShapeSelection == 1)
	{
		//
		// The center position (x, y, z) in world coordinates.
		//
		SdkUiInputVector("Origin", &m_CurrentCircle.Origin, 3, NULL);
		SdkUiForceOnSameLine();

		bool OriginClicked = false;
		SdkUiSmallButton("Local", &OriginClicked);
		if (OriginClicked)
		{
			//
			// Use local player's position.
			//
			SdkGetObjectPosition(_g_LocalPlayer, &m_CurrentCircle.Origin);
		}

		//
		// The direction of the circle's expansion.
		//
		SdkUiInputVector("Direction", &m_CurrentCircle.Direction, 3, NULL);

		//
		// The radius of the circle.
		//
		SdkUiInputFloat("Radius", &m_CurrentCircle.Radius, 3, NULL);

		//
		// The style of the circle.
		//
		static const char* Textures[] = { "Basic", "AoE" };
		SdkUiCombo("Texture", &m_CurrentCircle.Texture, Textures, RTL_NUMBER_OF(Textures), NULL);
	}
	//
	// Cone.
	//
	else if (ShapeSelection == 2)
	{
		//
		// The center position (x, y, z) in world coordinates.
		//
		SdkUiInputVector("Origin", &m_CurrentCone.Origin, 3, NULL);
		SdkUiForceOnSameLine();

		bool OriginClicked = false;
		SdkUiSmallButton("Local", &OriginClicked);
		if (OriginClicked)
		{
			//
			// Use local player's position.
			//
			SdkGetObjectPosition(_g_LocalPlayer, &m_CurrentCone.Origin);
		}

		//
		// The direction of the cone's expansion.
		//
		SdkUiInputVector("Direction", &m_CurrentCone.Direction, 3, NULL);

		//
		// The length of the cone.
		//
		SdkUiInputFloat("Length", &m_CurrentCone.Length, 3, NULL);

		//
		// The angle of the cone.
		//
		SdkUiDragFloat("Angle", &m_CurrentCone.Angle, 0, 360.f, "%.3f�", NULL);

		//
		// The style of the cone.
		//
		static const char* Textures[] = { "Cone" };
		SdkUiCombo("Texture", &m_CurrentCone.Texture, Textures, RTL_NUMBER_OF(Textures), NULL);
	}
	//
	// Text.
	//
	else if (ShapeSelection == 3)
	{
		//
		// The text position (x, y, z) in world coordinates.
		//
		SdkUiInputVector("Position", &m_CurrentText.Position, 3, NULL);
		SdkUiForceOnSameLine();

		bool PositionClicked = false;
		SdkUiSmallButton("Local", &PositionClicked);
		if (PositionClicked)
		{
			//
			// Use local player's position.
			//
			SdkGetObjectPosition(_g_LocalPlayer, &m_CurrentText.Position);
		}

		//
		// The font face, e.g. Arial, Verdana, Consolas, etc.
		//
		SdkUiInputText("Face", m_CurrentText.Face, RTL_NUMBER_OF(m_CurrentText.Face), 0, NULL);

		//
		// The height of the font.
		//
		SdkUiInputNumber("Height", &m_CurrentText.Height, NULL);

		//
		// The width of the font.
		//
		SdkUiInputNumber("Width", &m_CurrentText.Width, NULL);

		//
		// The boldness of the font.
		//
		SdkUiInputNumber("Weight", &m_CurrentText.Weight, NULL);

		//
		// If the font should be rendered as italic or not.
		//
		SdkUiCheckbox("Italic", &m_CurrentText.Italic, NULL);

		//
		// The actual contents to display.
		//
		static SDKPOINT TextboxSize = { -1.0f, 100.f };
		SdkUiInputMultiLineText("Message", m_CurrentText.Message, RTL_NUMBER_OF(m_CurrentText.Message), &TextboxSize, 0, NULL);
	}

	bool DrawClicked = false;
	SdkUiButton("Draw", &DrawClicked);
	if (DrawClicked)
	{
		//
		// Add the object to the appropriate set so it can be drawn.
		//

		SDKCOLOR Color;
		RGBAtoBGRA(m_CurrentColor, Color);

		//
		// Line.
		//
		if (ShapeSelection == 0)
		{
			m_CurrentLine.Color = Color;
			m_Lines.push_back(m_CurrentLine);
		}
		//
		// Circle.
		//
		else if (ShapeSelection == 1)
		{
			m_CurrentCircle.Color = Color;
			m_Circles.push_back(m_CurrentCircle);
		}
		//
		// Cone.
		//
		else if (ShapeSelection == 2)
		{
			m_CurrentCone.Color = Color;
			m_Cones.push_back(m_CurrentCone);
		}
		//
		// Text.
		//
		else if (ShapeSelection == 3)
		{
			m_CurrentText.Color = Color;
			m_Texts.push_back(m_CurrentText);
		}
	}

	//
	// Tree-like structure to display all currently shapes that are
	// being drawn per frame.
	//
	bool ShapesExpanded = false;
	SdkUiBeginTree("Shapes", &ShapesExpanded);

	if (ShapesExpanded)
	{
		//
		// Sub-tree for line objects.
		//
		bool LinesExpanded = false;
		SdkUiBeginTree("Lines", &LinesExpanded);

		if (LinesExpanded)
		{
			SdkUiText("There are %Iu lines.", m_Lines.size());

			size_t LineIndex = 0;
			for (auto it = m_Lines.begin(); it != m_Lines.end(); )
			{
				auto& Line = *it;

				bool LineExpanded = false;
				SdkUiBeginTreeEx(&LineExpanded, (void*)&Line, "Line: %Iu", ++LineIndex);

				bool DeleteClicked = false;

				if (LineExpanded)
				{
					//
					// Information about a drawn line.
					//
					SdkUiBulletText("Start: %f.%f.%f", Line.Start.x, Line.Start.y, Line.Start.z);
					SdkUiBulletText("End: %f.%f.%f", Line.End.x, Line.End.y, Line.End.z);
					SdkUiBulletText("Width: %f", Line.Width);
					SdkUiBulletText("Texture: %d", Line.Texture);
					SdkUiBulletText("Color: { R: %u, G: %u, B: %u, A: %u }", Line.Color.R, Line.Color.G, Line.Color.B, Line.Color.A);

					SdkUiBulletPoint();

					//
					// Remove the line from the set, preventing it 
					// from being drawn.
					//
					SdkUiSmallButton("Delete", &DeleteClicked);

					if (DeleteClicked)
						it = m_Lines.erase(it);

					SdkUiEndTree();
				}

				if (!DeleteClicked)
					++it;
			}

			//
			// Remove all lines from the set.
			//
			bool ClearLinesClicked = false;
			SdkUiButton("Clear", &ClearLinesClicked);
			if (ClearLinesClicked)
				m_Lines.clear();

			SdkUiEndTree();
		}

		//
		// Sub-tree for circle objects.
		//
		bool CirclesExpanded = false;
		SdkUiBeginTree("Circles", &CirclesExpanded);

		if (CirclesExpanded)
		{
			SdkUiText("There are %Iu circles.", m_Circles.size());

			size_t CircleIndex = 0;
			for (auto it = m_Circles.begin(); it != m_Circles.end(); )
			{
				auto& Circle = *it;

				bool CircleExpanded = false;
				SdkUiBeginTreeEx(&CircleExpanded, (void*)&Circle, "Circle: %Iu", ++CircleIndex);

				bool DeleteClicked = false;

				if (CircleExpanded)
				{
					//
					// Information about a drawn circle.
					//
					SdkUiBulletText("Origin: %f.%f.%f", Circle.Origin.x, Circle.Origin.y, Circle.Origin.z);
					SdkUiBulletText("Direction: %f.%f.%f", Circle.Direction.x, Circle.Direction.y, Circle.Direction.z);
					SdkUiBulletText("Radius: %f", Circle.Radius);
					SdkUiBulletText("Texture: %d", Circle.Texture);
					SdkUiBulletText("Color: { R: %u, G: %u, B: %u, A: %u }", Circle.Color.R, Circle.Color.G, Circle.Color.B, Circle.Color.A);

					SdkUiBulletPoint();

					//
					// Remove the circle from the set, preventing it 
					// from being drawn.
					//
					SdkUiSmallButton("Delete", &DeleteClicked);

					if (DeleteClicked)
						it = m_Circles.erase(it);
	
					SdkUiEndTree();
				}

				if (!DeleteClicked)
					++it;
			}

			//
			// Remove all circles from the set.
			//
			bool ClearCirclesClicked = false;
			SdkUiButton("Clear", &ClearCirclesClicked);
			if (ClearCirclesClicked)
				m_Circles.clear();

			SdkUiEndTree();
		}

		//
		// Sub-tree for cone objects.
		//
		bool ConesExpanded = false;
		SdkUiBeginTree("Cones", &ConesExpanded);

		if (ConesExpanded)
		{
			SdkUiText("There are %Iu cones.", m_Cones.size());

			size_t ConeIndex = 0;
			for (auto it = m_Cones.begin(); it != m_Cones.end(); )
			{
				auto& Cone = *it;

				bool ConeExpanded = false;
				SdkUiBeginTreeEx(&ConeExpanded, (void*)&Cone, "Cone: %Iu", ++ConeIndex);

				bool DeleteClicked = false;

				if (ConeExpanded)
				{
					//
					// Information about a drawn cone.
					//
					SdkUiBulletText("Origin: %f.%f.%f", Cone.Origin.x, Cone.Origin.y, Cone.Origin.z);
					SdkUiBulletText("Direction: %f.%f.%f", Cone.Direction.x, Cone.Direction.y, Cone.Direction.z);
					SdkUiBulletText("Length: %f", Cone.Length);
					SdkUiBulletText("Angle: %f", Cone.Angle);
					SdkUiBulletText("Color: { R: %u, G: %u, B: %u, A: %u }", Cone.Color.R, Cone.Color.G, Cone.Color.B, Cone.Color.A);
					SdkUiBulletText("Texture: %d", Cone.Texture);

					SdkUiBulletPoint();

					//
					// Remove the cone from the set, preventing it 
					// from being drawn.
					//
					SdkUiSmallButton("Delete", &DeleteClicked);

					if (DeleteClicked)
						it = m_Cones.erase(it);

					SdkUiEndTree();
				}

				if (!DeleteClicked)
					++it;
			}

			//
			// Remove all cones from the set.
			//
			bool ClearConesClicked = false;
			SdkUiButton("Clear", &ClearConesClicked);
			if (ClearConesClicked)
				m_Cones.clear();

			SdkUiEndTree();
		}

		//
		// Sub-tree for text messages.
		//
		bool TextsExpanded = false;
		SdkUiBeginTree("Texts", &TextsExpanded);

		if (TextsExpanded)
		{
			SdkUiText("There are %Iu texts.", m_Texts.size());

			size_t TextIndex = 0;
			for (auto it = m_Texts.begin(); it != m_Texts.end(); )
			{
				auto& Text = *it;

				bool TextExpanded = false;
				SdkUiBeginTreeEx(&TextExpanded, (void*)&Text, "Text: %Iu", ++TextIndex);

				bool DeleteClicked = false;

				if (TextExpanded)
				{
					//
					// Information about a drawn message.
					//
					SdkUiBulletText("Position: %f.%f.%f", Text.Position.x, Text.Position.y, Text.Position.z);
					SdkUiBulletText("Face: %s", Text.Face);
					SdkUiBulletText("Color: { R: %u, G: %u, B: %u, A: %u }", Text.Color.R, Text.Color.G, Text.Color.B, Text.Color.A);
					SdkUiBulletText("Height: %d", Text.Height);
					SdkUiBulletText("Width: %d", Text.Width);
					SdkUiBulletText("Weight: %d", Text.Weight);
					SdkUiBulletText("Italic: %u", Text.Italic);

					SdkUiBulletPoint();

					//
					// Remove the text from the set, preventing it 
					// from being drawn.
					//
					SdkUiSmallButton("Delete", &DeleteClicked);

					if (DeleteClicked)
						it = m_Texts.erase(it);

					SdkUiEndTree();
				}

				if (!DeleteClicked)
					++it;
			}

			//
			// Remove all texts from the set.
			//
			bool ClearTextsClicked = false;
			SdkUiButton("Clear", &ClearTextsClicked);
			if (ClearTextsClicked)
				m_Texts.clear();

			SdkUiEndTree();
		}

		SdkUiEndTree();
	}

	//
	// Signal that we're done displaying this window.
	//
	SdkUiEndWindow();
}

void Drawing::DrawGame(
	void
)
/*++

Routine Description:

	This function is invoked each frame.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// Draw all the shapes in our line, circle, and cone sets.
	//

	//
	// Check to see if we're drawing the current waypoint...
	//
	if (m_DrawWaypoint)
	{
		//
		// Get the movement data for this frame.
		//

		size_t NextWaypoint = 0;
		size_t NumberOfWaypoints = 0;
		PSDKVECTOR Waypoints = NULL;
		bool IsDash = false;
		float DashSpeed = 0;
		float DashGravity = 0;
		SdkGetAINavData(_g_LocalPlayer, NULL, NULL, &NextWaypoint, &NumberOfWaypoints, &Waypoints, NULL, &IsDash, &DashSpeed, &DashGravity);

		//
		// It's not enough to just check the pointer (Waypoints) against NULL.
		// You must also check the number (NumberOfWaypoints) variable.
		//
		if (Waypoints && NumberOfWaypoints)
		{
			SDKCOLOR Color;
			RGBAtoBGRA(m_CurrentColor, Color);

			SDKVECTOR Current;
			SdkGetObjectPosition(_g_LocalPlayer, &Current);

			//
			// Draw a line from our local player's position to the next
			// waypoint.
			//

			if (IsDash)
			{
				std::string Dashing = ">> DASHING: " + std::to_string(DashSpeed) + ", " + std::to_string(DashGravity) + " <<\n\n\n";
				SdkDrawText(&Current, NULL, Dashing.c_str(), "Arial Narrow", &Color, 14, 0, 0, false);
			}

			PSDKVECTOR Waypoint = &Waypoints[NextWaypoint];
			SdkDrawLine(&Current, Waypoint, 10.f, &Color, 0);
		}
	}

	for (auto& Line : m_Lines)
	{
		SdkDrawLine(&Line.Start, &Line.End, Line.Width, &Line.Color, Line.Texture);
	}

	for (auto& Circle : m_Circles)
	{
		SdkDrawCircle(&Circle.Origin, Circle.Radius, &Circle.Color, Circle.Texture, &Circle.Direction);
	}

	for (auto& Cone : m_Cones)
	{
		SdkDrawCone(&Cone.Origin, Cone.Length, Cone.Angle, &Cone.Direction, &Cone.Color, Cone.Texture);
	}

	for (auto& Text : m_Texts)
	{
		SdkDrawText(&Text.Position, NULL, Text.Message, Text.Face, &Text.Color, Text.Height, Text.Width, Text.Weight, Text.Italic);
	}
}
