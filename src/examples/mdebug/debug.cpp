/*++

Module Name:

   debug.cpp

Abstract:

	This module showcases how to use the Rift API to:
		* access the local player object,
		* render hack menus to display information and take in user 
		  input,
		* display in-game visuals,
		* convert world to screen coordinates and vice-versa,
		* iterate over game objects such as attackable units,
		  heroes, minions, turrets, and spell missiles,
		* control local player (and pet) input to issue movement,
		  attack, spell cast, emote, and chat commands
	
	This is considered an 'all-in-one' module for Rift developers to
	use in troubleshooting and creating their own derivatives. 

--*/

#include "stdafx.h"
#include "debug.h"
#include "coords.h"
#include "drawing.h"
#include "gameobjects.h"
#include "input.h"
#include "settings.h"
#include "resource.h"

//
// This stores a pointer to all the SDK functions we can use. This is 
// intentionally a global so that other functions can use it.
//
PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

//
// The local player object never changes. We can store it once after we
// retrieve it from the SDK.
//
void* _g_LocalPlayer = NULL;

BOOL
WINAPI
DllMain(
	_In_ HINSTANCE hinstDLL,
	_In_ DWORD fdwReason,
	_In_ LPVOID lpvReserved
)
/*++

Routine Description:

	This function is called when the SDK loads the module. Consider
	this your module entry point.

Arguments:

	hinstDLL - The same value as lpvReserved when loaded by the SDK.

	fdwReason - Exclusively DLL_PROCESS_ATTACH (1) when loaded by the
		SDK.

	lpvReserved - The same value as hinstDLL when loaded by the SDK.
		Contains a pointer to the SDK_CONTEXT structure that can be
		retrieved via the SDK_EXTRACT_CONTEXT macro. This structure
		contains all the necessary function pointers required to
		interface with the Rift SDK.

Return Value:

	'TRUE' indicates module load success. 'FALSE' indicates module
	load failure.

--*/
{
	//
	// This parameter is unused.
	//
	UNREFERENCED_PARAMETER(hinstDLL);

	//
	// We're only interested when the DLL attaches to the process.
	//
	if (fdwReason != DLL_PROCESS_ATTACH)
		return TRUE;

	//
	// This macro extracts the pointer to the SDK context from the lpvReserved
	// parameter.
	//
	SDK_EXTRACT_CONTEXT(lpvReserved);
	if (!SDK_CONTEXT_GLOBAL)
		return FALSE;

	//
	// Every module loaded by Rift should have a call to SdkNotifyLoadedModule
	// as soon as possible to ensure that other SDK API can be called.
	//
	if (!SDKSTATUS_SUCCESS(SdkNotifyLoadedModule("Debug", SDK_VERSION)))
	{
		//
		// This routine will fail if a module of the same name is already
		// loaded or if there's a mismatch with the target SDK version.
		//
		return FALSE;
	}

	//
	// In order to use basically any SDK function (aside from 
	// SdkNotifyLoadedModule and SdkRegisterOnLoad), we need to be
	// in the main game thread. 
	//
	// All callbacks are executed in the context of the main game 
	// thread. Calling SdkRegisterOnLoad here ensures that we can 
	// use other SDK API safely as it will execute in the main
	// game thread.
	//
	SdkRegisterOnLoad
	(
		[](void* UserData) -> void
		{
			UNREFERENCED_PARAMETER(UserData);

			//
			// Retrieve the local player object.
			//
			if (!SDKSTATUS_SUCCESS(SdkGetLocalPlayer(&_g_LocalPlayer)) || !_g_LocalPlayer)
			{
				SdkUiConsoleWrite("[error] Could not retrieve the local player unit.\n");
				return;
			}

			//
			// Get information about the current game.
			//
			__int64 GameID;
			const char* Mode;
			unsigned int MapID;
			SdkGetGameData(&MapID, &Mode, &GameID);

			SdkUiConsoleWrite("[mdebug] Game: %lld || Map: %u || Mode: %s.\n", GameID, MapID, Mode);

			//
			// Initialize the standalone 'features' of the debug module.
			//
			Coordinates::Initialize();
			Drawing::Initialize();
			GameObjects::Initialize();
			Input::Initialize();
			Settings::Initialize();

			//
			// When the overlay is being drawn (e.g. hack menu is up), invoke
			// our function.
			//
			SdkRegisterOverlayScene(DrawOverlayScene, NULL);

			//
			// Each frame, the SDK will call into this function where we can 
			// add draw additional visuals before it's dispatched to the GPU
			// to render.
			//
			SdkRegisterGameScene(DrawGameScene, NULL);

			//
			// The base 'game frame' sits below our extra visuals (the 'game
			// scene'). The 'overlay scene' sits atop both of these, but is
			// only visible when the end user presses the required hotkey.
			//
		}, NULL
	);

	return TRUE;
}

void 
__cdecl 
DrawGameScene(
	_In_ void* UserData
)
/*++

Routine Description:

	This function is invoked every frame.

Arguments:

	UserData - A pointer to an arbitrary data structure, provided
		by the user, when the overlay scene was registered.

Return Value:

	None.

--*/
{
	UNREFERENCED_PARAMETER(UserData);

	//
	// Each frame, we draw extra visuals onto the game frame.
	//
	Drawing::DrawGame();

	//
	// Get a handle to the game window.
	//
	HWND Window = NULL;
	SdkUiGetWindow(&Window);

	//
	// Find the dimensions of the game window.
	//
	RECT Rect = { 0 };
	GetClientRect(Window, &Rect);

	//
	// Draw the logo at the bottom left corner of the game window.
	//
	SDKPOINT ScreenPoint = { 25.f, Rect.bottom - 100.f };

	//
	// The resource being loaded must be of type RT_BITMAP or
	// RT_RCDATA in your *.rc file. You can determine this type 
	// by viewing the code of your *.rc file and ensuring a line
	// similar to the one below appears:
	//	
	//	IDB_LOGO		RCDATA		"assets\\logo_text.png"
	//					^^^^^^
	// This must be RCDATA (or BITMAP if you are loading a .BMP). If it
	// is not, just change it manually in your *.rc file.	
	//
	// The name, IDB_LOGO, should be identical in both your resource 
	// header file (.h) and your *.rc file.
	//
	SdkDrawSpriteFromResource(MAKEINTRESOURCEA(IDB_LOGO), &ScreenPoint, false);
}

void
__cdecl
DrawOverlayScene(
	_In_ void* UserData
)
/*++

Routine Description:

	This function is invoked every frame when the overlay is visible.

Arguments:

	UserData - A pointer to an arbitrary data structure, provided
		by the user, when the overlay scene was registered.

Return Value:

	None.

--*/
{
	UNREFERENCED_PARAMETER(UserData);

	//
	// If the overlay is visible, draw the following UI elements.
	//

	if (Coordinates::ShowWindow)
		Coordinates::Draw();

	if (Drawing::ShowWindow)
		Drawing::DrawOverlay();

	if (GameObjects::ShowWindow)
		GameObjects::Draw();

	if (Input::ShowWindow)
		Input::Draw();

	if (Settings::ShowWindow)
		Settings::Draw();

	//
	// Initially, a 'Debug' window is shown with several buttons.
	// Clicking on a particular button will create another window
	// with specific UI elements related to the target feature.
	//

	bool CoordinatesClicked = false;
	SdkUiButton("Coordinates", &CoordinatesClicked);
	if (CoordinatesClicked)
		Coordinates::ShowWindow = true;

	bool DrawClicked = false;
	SdkUiButton("Draw", &DrawClicked);
	if (DrawClicked)
		Drawing::ShowWindow = true;

	bool GameObjectsClicked = false;
	SdkUiButton("Game Objects", &GameObjectsClicked);
	if (GameObjectsClicked)
		GameObjects::ShowWindow = true;

	bool InputClicked = false;
	SdkUiButton("Input", &InputClicked);
	if (InputClicked)
		Input::ShowWindow = true;

	bool SettingsClicked = false;
	SdkUiButton("Settings", &SettingsClicked);
	if (SettingsClicked)
		Settings::ShowWindow = true;
}