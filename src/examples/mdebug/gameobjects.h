/*++

Module Name:

   GameObjects.h

Abstract:

	Utilizes the Rift SDK to retrieve information about all the
	active game-objects.

--*/

#pragma once

///
/// Classes.
///

class GameObjects
{
private:
	//
	// For attackable objects.
	//
	// Chain of inheritance:
	//		AttackableUnit -> GameObject
	//
	struct AttackableUnit
	{
		bool Visible;
		float TimeOfDeath;
		SDK_ABILITY_RESOURCE AbilityResources[2];
		bool MouseOver;
		SDK_HEALTH Health;
		bool Invulnerable;
		int Targetability;
	};

	struct BuffInstance
	{
		unsigned char Type;
		float StartTime;
		float EndTime;
		std::string Name;
		void* CasterObject;
		unsigned int CasterID;
		int Stacks;
		bool HasCount;
		int Count;
		SDK_SPELL Spell;
	};

	//
	// For artificially intelligent objects.
	//
	// Chain of inheritance:
	//		AIBaseClient -> AttackableUnit -> GameObject
	//
	struct AIBaseClient
	{
		int SkinID;
		bool PlayerControlled;
		bool CanAttack;
		bool CanCrit;
		bool CanCast;
		bool CanMove;
		bool Stealthed;
		bool Taunted;
		bool Feared;
		bool Fleeing;
		bool Suppressed;
		bool Sleeping;
		bool NearSighted;
		bool Ghosted;
		bool Charmed;
		bool Slowed;
		bool Selectable;
		bool CritImmune;
		bool Grounded;
		bool Obscured;
		bool Killable;
		float Armor;
		float BonusArmor;
		float Lethality;
		float FlatArmorPenetration;
		float PercentArmorPenetration;
		float BonusArmorPenetration;
		float AttackSpeed;
		float AbilityPower;
		float AttackDamage;
		float AttackRange;
		float MagicResist;
		float BonusMagicResist;
		float Tenacity;
		float MovementSpeed;
		float CritChance;
		float CritDamageMultiplier;
		float DodgeChance;
		float HealthRegen;
		float FlatMagicPenetration;
		float PercentMagicPenetration;
		float LifeSteal;
		float SpellVamp;
		float AbilityResourceRegens[2];
		float CooldownReduction;
		bool Bot;
		float CurrentGold;
		float TotalGold;
		std::vector<BuffInstance> Buffs;
		int CombatType;
		std::string Name;

		struct SDK_SPELL_EXTENDED
		{
			SDK_SPELL Spell;
			bool Castable;
			std::string Name;
			std::string ScriptName;
			std::string AlternateName;
			std::string DisplayName;
			std::string Description;
			std::string AnimationName;
			std::string MissileEffectName;
		};

		std::vector<SDK_SPELL_EXTENDED> SpellsEx;
		SDK_SPELL BasicAttack;
		float DeathDuration;
		bool Attacking;
		bool Moving;
		SDKVECTOR NavStartWorldPosition;
		SDKVECTOR NavEndWorldPosition;
		size_t NavNextWaypoint;
		std::vector<SDKVECTOR> NavWaypoints;
		SDKVECTOR NavVelocity;
		bool Dashing;
		float DashSpeed;
		float DashGravity;
		bool Casting;
		
		struct SDK_SPELL_CAST_EXTENDED
		{
			bool Valid;
			SDK_SPELL_CAST SpellCast;
			void* ParticleObject;
			float EndTime;
			bool SpellWasCast;
			bool IsCharging;
			bool IsChanneling;
			float ChannelEndTime;
			bool IsStopped;
		} ActiveSpell;

		unsigned int EnemyID;
		float AttackDelay;
		float AttackCastDelay;
		float BaseAttackDelay;
		float BaseAttackDamage;
		float BonusAttackDamage;
		SDKVECTOR HealthbarWorldPosition;
		SDKVECTOR FacingDirection;
		SDKPOINT HealthbarScreenPosition;
		SDKVECTOR ServerPosition;
	};

	struct PerkInstance
	{
		unsigned int ID;
		std::string Name;
	};

	//
	// For hero (champion) objects.
	//
	// Chain of inheritance:
	//		AIHeroClient -> AIBaseClient -> AttackableUnit -> GameObject
	//
	struct AIHeroClient
	{
		float Experience;
		int Level;
		int NeutralKills;

		struct SDK_ITEM_EXTENDED
		{
			SDK_ITEM Item;
			std::string DisplayName;
		};

		std::vector<SDK_ITEM_EXTENDED> ItemsEx;
		std::vector<PerkInstance> Perks;
	};

	//
	// For minion objects.
	//
	// Chain of inheritance:
	//		AIMinionClient -> AIBaseClient -> AttackableUnit -> GameObject
	//
	struct AIMinionClient
	{
		float SpawnTime;
		int CampNumber;
		bool LaneMinion;
		int Type;
		int Level;
		bool Ward;
	};

	//
	// For turret (tower) objects.
	//
	// Chain of inheritance:
	//		AITurretClient -> AIBaseClient -> AttackableUnit -> GameObject
	//
	struct AITurretClient
	{
		int Position;
		int Lane;
	};

	//
	// For spell missile objects.
	//
	// Chain of inheritance:
	//		MissileClient -> GameObject
	//
	struct MissileClient
	{
		SDK_SPELL Spell;
		unsigned int CasterID;
		SDKVECTOR StartWorldPosition;
		bool AutoAttack;
		float Speed;
		SDKVECTOR TargetWorldPosition;
		unsigned int TargetID;
		bool Completed;
		float StartTime;
		float Width;
		unsigned int ParentMissileID;
	};

	//
	// For particle objects.
	//
	// Chain of inheritance:
	//		ParticleClient -> GameObject
	//
	struct ParticleClient
	{
		void* Owner;
	};

	//
	// Every object has, at the very minimum, this base structure.
	//
	// Chain of inheritance:
	//		GameObject
	//
	struct GameObject
	{
		void* Pointer;
		int TypeFlags;
		int TypeID;
		std::string TypeName;
		unsigned int ID;
		unsigned int NetworkID;
		int TeamID;
		SDKVECTOR Position;
		bool Zombie;
		bool Dead;
		std::string Name;
		SDKVECTOR Velocity;
		SDKVECTOR Acceleration;
		SDKVECTOR Orientation;
		SDKBOX BBox;
		float BRadius;
		bool VisibleOnScreen;
		unsigned int Respawns;
		std::shared_ptr<AttackableUnit> Unit;
		std::shared_ptr<AIBaseClient> AI;
		std::shared_ptr<AIHeroClient> Hero;
		std::shared_ptr<AIMinionClient> Minion;
		std::shared_ptr<AITurretClient> Turret;
		std::shared_ptr<MissileClient> Missile;
		std::shared_ptr<ParticleClient> Particle;
	};

	static std::map<std::string, std::vector<GameObject>> m_ObjectMapping;
	static SDKCOLOR m_ColorTeal;

public: 
	static bool ShowWindow;

	static void Initialize(
		void
	);

	static void Draw(
		void
	);
};
