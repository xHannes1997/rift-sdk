/*++

Module Name:

   stdafx.h

Abstract:

	The precompiled header: compiled into an intermediate form that
	is faster to process for the compiler.

--*/

#pragma once

//
// This lets us use Windows types, structures, and defined API.
//
#include <Windows.h>

//
// To use std::shared_ptr.
//
#include <memory>

//
// To use std::map.
//
#include <map>

//
// To use std::vector.
//
#include <vector>

//
// To use std::string.
//
#include <string>

//
// In order to use the Rift API, we include this.
//
#include "../../sdkapi.h"
