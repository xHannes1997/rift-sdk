/*++

Module Name:

   coords.h

Abstract:

	Demonstrates how to use the Rift SDK to convert screen coordinates
	(x, y) to world coordinates (x, y, z) and vice-versa.

--*/

#pragma once

///
/// Classes.
///

class Coordinates
{
private:
	static SDKPOINT m_CurrentScreenCoordinates;
	static SDKVECTOR m_CurrentWorldCoordinates;
	
public:
	static bool ShowWindow;

	static void Initialize(
		void
	);

	static void Draw(
		void
	);
};
