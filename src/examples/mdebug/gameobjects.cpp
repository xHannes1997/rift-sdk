/*++

Module Name:

   GameObjects.cpp

Abstract:

	Utilizes the Rift SDK to retrieve information about all the
	active game-objects.

--*/

#include "stdafx.h"
#include "gameobjects.h"
#include "debug.h"

//
// Whether the window is currently shown in the overlay.
//
bool GameObjects::ShowWindow = false;

//
// The set of all objects and their state at a particular game frame.
//
std::map<std::string, std::vector<GameObjects::GameObject>> GameObjects::m_ObjectMapping;

SDKCOLOR GameObjects::m_ColorTeal;

void GameObjects::Initialize(
	void
)
/*++

Routine Description:

	Initializes class specific data structures.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// Teal.
	//
	m_ColorTeal.B = 181;
	m_ColorTeal.G = 167;
	m_ColorTeal.R = 24;
	m_ColorTeal.A = 255;
}

void GameObjects::Draw(
	void
)
/*++

Routine Description:

	This function is invoked each frame while the overlay is present
	and the target window is being shown.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// Get the previous window's location and size.
	//
	SDKPOINT Position, Size;
	SdkUiGetWindowDim(&Position, &Size);

	// 
	// Set this new window to display directly to the right of the 
	// previous window.
	//
	Position.x += (Size.x + 10.f);
	SdkUiSetNextWindowPos(&Position);

	//
	// Create the new window.
	//
	bool Collapsed = false;
	SdkUiBeginWindow("Game Objects", &GameObjects::ShowWindow, &Collapsed);
	if (!Collapsed)
	{
		//
		// Early out here if the window is partially non-visible.
		//
		SdkUiEndWindow();
		return;
	}

	bool Refresh = false;
	SdkUiButton("Refresh", &Refresh);

	//
	// NOTE: This is marked as 'static' so it's effectively a global
	// variable and therefore won't be reset to '0' each frame.
	//
	static int NumberOfObjects = 0;

	//
	// We retrieve the current state of the game when the button is 
	// pressed.
	//
	// NOTE: This data is stale almost immediately as it's only ever 
	// truly accurate in the first frame it was retrieved.
	//
	if (Refresh)
	{
		SdkGetNumberOfGameObjects(&NumberOfObjects);

		m_ObjectMapping.clear();

		//
		// We store all the data we want for an object here. This is 
		// necessary because we do not update objects in realtime, 
		// otherwise it would be difficult to view the object's 
		// properties as it will change constantly.
		//
		SdkEnumGameObjects
		(
			[](void* Object, unsigned int NetworkID, void* UserData) -> bool
			{
				UNREFERENCED_PARAMETER(UserData);

				//
				// A copy of the state of this object in the frame it
				// was retrieved.
				//
				GameObject CachedObject;
				CachedObject.Pointer = Object;
				CachedObject.NetworkID = NetworkID;
				SdkGetObjectTypeFlags(Object, &CachedObject.TypeFlags);
				SdkGetObjectTypeID(Object, &CachedObject.TypeID);

				//
				// NOTE: This intentionally does a duplication of the
				// string. Deep copies are required as accessing a 
				// dangling pointer past the lifetime of the object 
				// is a recipe for disaster (crash).
				//
				const char* TypeName = NULL;
				SdkGetObjectTypeName(Object, &TypeName);
				CachedObject.TypeName = TypeName;

				SdkGetObjectID(Object, &CachedObject.ID);
				SdkGetObjectTeamID(Object, &CachedObject.TeamID);
				SdkGetObjectPosition(Object, &CachedObject.Position);
				SdkIsObjectZombie(Object, &CachedObject.Zombie);
				SdkIsObjectDead(Object, &CachedObject.Dead);

				//
				// NOTE: This intentionally does a duplication of the
				// string.
				//
				const char* Name = NULL;
				SdkGetObjectName(Object, &Name);
				CachedObject.Name = Name;

				SdkGetObjectVelocity(Object, &CachedObject.Velocity);
				SdkGetObjectAcceleration(Object, &CachedObject.Acceleration);
				SdkGetObjectOrientation(Object, &CachedObject.Orientation);
				SdkGetObjectBoundingBox(Object, &CachedObject.BBox);
				SdkGetObjectBoundingRadius(Object, &CachedObject.BRadius);
				SdkIsObjectVisibleOnScreen(Object, &CachedObject.VisibleOnScreen);
				SdkGetObjectRespawns(Object, &CachedObject.Respawns);

				if (SDKSTATUS_SUCCESS(SdkIsObjectUnit(Object)))
				{
					//
					// Retrieve state about attackable unit objects.
					//
					CachedObject.Unit = std::make_shared<AttackableUnit>();
					SdkIsUnitVisible(Object, &CachedObject.Unit->Visible);
					SdkGetUnitDeathTime(Object, &CachedObject.Unit->TimeOfDeath);
					SdkGetUnitAbilityResource(Object, ABILITY_SLOT_PRIMARY, &CachedObject.Unit->AbilityResources[ABILITY_SLOT_PRIMARY]);
					SdkGetUnitAbilityResource(Object, ABILITY_SLOT_SECONDARY, &CachedObject.Unit->AbilityResources[ABILITY_SLOT_SECONDARY]);
					SdkGetUnitIsMouseOver(Object, &CachedObject.Unit->MouseOver);
					SdkGetUnitHealth(Object, &CachedObject.Unit->Health);
					SdkIsUnitInvulnerable(Object, &CachedObject.Unit->Invulnerable);
					SdkGetUnitTargetability(Object, &CachedObject.Unit->Targetability);
				}
				else
				{
					CachedObject.Unit = NULL;
				}

				if (SDKSTATUS_SUCCESS(SdkIsObjectAI(Object)))
				{
					//
					// Retrieve state about artificially intelligent objects.
					//
					CachedObject.AI = std::make_shared<AIBaseClient>();
					SdkGetAISkinID(Object, &CachedObject.AI->SkinID);
					SdkIsAIPlayerControlled(Object, &CachedObject.AI->PlayerControlled);
					SdkCanAIAttack(Object, &CachedObject.AI->CanAttack);
					SdkCanAICrit(Object, &CachedObject.AI->CanCrit);
					SdkCanAICast(Object, &CachedObject.AI->CanCast);
					SdkCanAIMove(Object, &CachedObject.AI->CanMove);
					SdkIsAIStealthed(Object, &CachedObject.AI->Stealthed);
					SdkIsAITaunted(Object, &CachedObject.AI->Taunted);
					SdkIsAIFeared(Object, &CachedObject.AI->Feared);
					SdkIsAIFleeing(Object, &CachedObject.AI->Fleeing);
					SdkIsAISuppressed(Object, &CachedObject.AI->Suppressed);
					SdkIsAISleeping(Object, &CachedObject.AI->Sleeping);
					SdkIsAINearSighted(Object, &CachedObject.AI->NearSighted);
					SdkIsAIGhosted(Object, &CachedObject.AI->Ghosted);
					SdkIsAICharmed(Object, &CachedObject.AI->Charmed);
					SdkIsAISlowed(Object, &CachedObject.AI->Slowed);
					SdkIsAISelectable(Object, &CachedObject.AI->Selectable);
					SdkIsAICritImmune(Object, &CachedObject.AI->CritImmune);
					SdkIsAIGrounded(Object, &CachedObject.AI->Grounded);
					SdkIsAIObscured(Object, &CachedObject.AI->Obscured);
					SdkIsAIKillable(Object, &CachedObject.AI->Killable);
					SdkGetAIArmor(Object, &CachedObject.AI->Armor);
					SdkGetAIBonusArmor(Object, &CachedObject.AI->BonusArmor);
					SdkGetAILethality(Object, &CachedObject.AI->Lethality);
					SdkGetAIArmorPenetration(Object, &CachedObject.AI->FlatArmorPenetration, &CachedObject.AI->PercentArmorPenetration);
					SdkGetAIBonusArmorPenetration(Object, &CachedObject.AI->BonusArmorPenetration);
					SdkGetAIAttackSpeed(Object, &CachedObject.AI->AttackSpeed);
					SdkGetAIAbilityPower(Object, &CachedObject.AI->AbilityPower);
					SdkGetAIAttackDamage(Object, &CachedObject.AI->AttackDamage);
					SdkGetAIAttackRange(Object, &CachedObject.AI->AttackRange);
					SdkGetAIMagicResist(Object, &CachedObject.AI->MagicResist);
					SdkGetAIBonusMagicResist(Object, &CachedObject.AI->BonusMagicResist);
					SdkGetAITenacity(Object, &CachedObject.AI->Tenacity);
					SdkGetAIMovementSpeed(Object, &CachedObject.AI->MovementSpeed);
					SdkGetAICritChance(Object, &CachedObject.AI->CritChance);
					SdkGetAICritDamageMultiplier(Object, &CachedObject.AI->CritDamageMultiplier);
					SdkGetAIDodgeChance(Object, &CachedObject.AI->DodgeChance);
					SdkGetAIHealthRegen(Object, &CachedObject.AI->HealthRegen);
					SdkGetAIMagicPenetration(Object, &CachedObject.AI->FlatMagicPenetration, &CachedObject.AI->PercentMagicPenetration);
					SdkGetAILifeSteal(Object, &CachedObject.AI->LifeSteal);
					SdkGetAISpellVamp(Object, &CachedObject.AI->SpellVamp);
					SdkGetAIAbilityResourceRegen(Object, ABILITY_SLOT_PRIMARY, &CachedObject.AI->AbilityResourceRegens[ABILITY_SLOT_PRIMARY]);
					SdkGetAIAbilityResourceRegen(Object, ABILITY_SLOT_SECONDARY, &CachedObject.AI->AbilityResourceRegens[ABILITY_SLOT_SECONDARY]);
					SdkGetAICooldownReduction(Object, &CachedObject.AI->CooldownReduction);
					SdkIsAIBot(Object, &CachedObject.AI->Bot);
					SdkGetAIGold(Object, &CachedObject.AI->CurrentGold, &CachedObject.AI->TotalGold);
					SdkEnumAIBuffs
					(
						Object, 
						[](unsigned char Type, float StartTime, float EndTime, const char* Name, void* CasterObject, unsigned int CasterID, int Stacks, bool HasCount, int Count, PSDK_SPELL Spell, void* UserData) -> bool
						{
							//
							// This callback is invoked for every buff
							// attached to this AI object.
							//

							std::vector<BuffInstance>* Buffs = (std::vector<BuffInstance>*)UserData;
	
							BuffInstance Buff;
							Buff.Type = Type;
							Buff.StartTime = StartTime;
							Buff.EndTime = EndTime;

							//
							// NOTE: This intentionally does a duplication of the
							// string.
							//
							Buff.Name = Name;
							Buff.CasterObject = CasterObject;
							Buff.CasterID = CasterID;
							Buff.Stacks = Stacks;
							Buff.HasCount = HasCount;
							Buff.Count = Count;
							
							if (Spell)
								memcpy(&Buff.Spell, Spell, sizeof(Buff.Spell));

							Buffs->push_back(Buff);
							
							return true;
						}, 
						&CachedObject.AI->Buffs
					);

					SdkGetAICombatType(Object, &CachedObject.AI->CombatType);
					
					//
					// NOTE: This intentionally does a duplication of the
					// string.
					//
					SdkGetAIName(Object, &Name);
					CachedObject.AI->Name = Name;

					for (uint8_t i = 0; i < SPELL_SLOT_MAX; ++i)
					{
						GameObjects::AIBaseClient::SDK_SPELL_EXTENDED SpellEx;
						SdkGetAISpell(Object, i, &SpellEx.Spell);
						
						if (!SpellEx.Spell.Learned)
							continue;

						int SpellCanCastFlags;
						SdkCanAICastSpell(Object, i, NULL, &SpellCanCastFlags);
						SpellEx.Castable = (SpellCanCastFlags == SPELL_CAN_CAST_OK);

						SpellEx.Name = SpellEx.Spell.Name;
						SpellEx.ScriptName = SpellEx.Spell.ScriptName;
						SpellEx.AlternateName = SpellEx.Spell.AlternateName;
						SpellEx.DisplayName = SpellEx.Spell.DisplayName;
						SpellEx.Description = SpellEx.Spell.Description;
						SpellEx.AnimationName = SpellEx.Spell.AnimationName;
						SpellEx.MissileEffectName = SpellEx.Spell.MissileEffectName;

						CachedObject.AI->SpellsEx.push_back(SpellEx);
					}

					SdkGetAIDeathDuration(Object, &CachedObject.AI->DeathDuration);
					SdkIsAIAttacking(Object, &CachedObject.AI->Attacking);
					SdkIsAIMoving(Object, &CachedObject.AI->Moving);

					//
					// NOTE: This is an SDKVECTOR*, intentionally.
					//
					PSDKVECTOR Waypoints = NULL;
					size_t NumberOfWaypoints = 0;
					SdkGetAINavData(Object, &CachedObject.AI->NavStartWorldPosition, &CachedObject.AI->NavEndWorldPosition, &CachedObject.AI->NavNextWaypoint, &NumberOfWaypoints, &Waypoints, &CachedObject.AI->NavVelocity, &CachedObject.AI->Dashing, &CachedObject.AI->DashSpeed, &CachedObject.AI->DashGravity);

					if (Waypoints && NumberOfWaypoints)
					{
						//
						// NOTE: This is a deep copy and is an intentional duplication.
						//
						CachedObject.AI->NavWaypoints.resize(NumberOfWaypoints);
						memcpy(CachedObject.AI->NavWaypoints.data(), Waypoints, (NumberOfWaypoints * sizeof(SDKVECTOR)));
					}

					SdkIsAICasting(Object, &CachedObject.AI->Casting);
					SdkGetAIActiveSpell(Object, &CachedObject.AI->ActiveSpell.Valid, &CachedObject.AI->ActiveSpell.SpellCast, &CachedObject.AI->ActiveSpell.ParticleObject, &CachedObject.AI->ActiveSpell.EndTime, &CachedObject.AI->ActiveSpell.SpellWasCast, &CachedObject.AI->ActiveSpell.IsCharging, &CachedObject.AI->ActiveSpell.IsChanneling, &CachedObject.AI->ActiveSpell.ChannelEndTime, &CachedObject.AI->ActiveSpell.IsStopped);

					SdkGetAIEnemy(Object, NULL, &CachedObject.AI->EnemyID);
					SdkGetAIAttackDelay(Object, &CachedObject.AI->AttackDelay);
					SdkGetAIAttackCastDelay(Object, &CachedObject.AI->AttackCastDelay);
					SdkGetAIBasicAttack(Object, &CachedObject.AI->BasicAttack);
					SdkGetAIBaseAttackDamage(Object, &CachedObject.AI->BaseAttackDamage);
					SdkGetAIBonusAttackDamage(Object, &CachedObject.AI->BonusAttackDamage);
					SdkGetAIHealthbarWorldPosition(Object, &CachedObject.AI->HealthbarWorldPosition);
					SdkGetAIFacingDirection(Object, &CachedObject.AI->FacingDirection);
					SdkGetAIHealthbarScreenPosition(Object, &CachedObject.AI->HealthbarScreenPosition);
					SdkGetAIServerPosition(Object, &CachedObject.AI->ServerPosition);
				}
				else
				{
					CachedObject.AI = NULL;
				}
				
				if (SDKSTATUS_SUCCESS(SdkIsObjectHero(Object)))
				{
					//
					// Retrieve state about hero (champion) objects.
					//
					CachedObject.Hero = std::make_shared<AIHeroClient>();
					SdkGetHeroExperience(Object, &CachedObject.Hero->Experience, &CachedObject.Hero->Level);
					SdkGetHeroNeutralKills(Object, &CachedObject.Hero->NeutralKills);

					for (uint8_t i = 0; i < ITEM_SLOT_MAX; ++i)
					{
						GameObjects::AIHeroClient::SDK_ITEM_EXTENDED ItemEx;
						SdkGetHeroItem(Object, i, &ItemEx.Item);

						if (!ItemEx.Item.Count)
							continue;

						ItemEx.DisplayName = ItemEx.Item.DisplayName;

						CachedObject.Hero->ItemsEx.push_back(ItemEx);
					}

					SdkEnumHeroPerks
					(
						Object,
						[](unsigned int ID, const char* Name, void* UserData) -> bool
						{
							//
							// This callback is invoked for every perk
							// attached to this hero.
							//
							std::vector<PerkInstance>* Perks = (std::vector<PerkInstance>*)UserData;

							PerkInstance Perk;
							Perk.ID = ID;
							Perk.Name = Name;

							Perks->push_back(Perk);

							return true;
						},
						&CachedObject.Hero->Perks
					);

				}
				else
				{
					CachedObject.Hero = NULL;
				}

				if (SDKSTATUS_SUCCESS(SdkIsObjectMinion(Object)))
				{
					//
					// Retrieve state about minion objects.
					//
					CachedObject.Minion = std::make_shared<AIMinionClient>();
					SdkGetMinionSpawnTime(Object, &CachedObject.Minion->SpawnTime);
					SdkGetMinionCampNumber(Object, &CachedObject.Minion->CampNumber);
					SdkIsMinionLaneMinion(Object, &CachedObject.Minion->LaneMinion);
					SdkGetMinionType(Object, &CachedObject.Minion->Type);
					SdkGetMinionLevel(Object, &CachedObject.Minion->Level);
					SdkIsMinionWard(Object, &CachedObject.Minion->Ward);
				}
				else
				{
					CachedObject.Minion = NULL;
				}

				if (SDKSTATUS_SUCCESS(SdkIsObjectTurret(Object)))
				{
					//
					// Retrieve state about turret (tower) objects.
					//
					CachedObject.Turret = std::make_shared<AITurretClient>();
					SdkGetTurretInfo(Object, &CachedObject.Turret->Position, &CachedObject.Turret->Lane);
				}
				else
				{
					CachedObject.Turret = NULL;
				}

				if (SDKSTATUS_SUCCESS(SdkIsObjectSpellMissile(Object)))
				{
					CachedObject.Missile = std::make_shared<MissileClient>();
					SdkGetMissileSpell(Object, &CachedObject.Missile->Spell);
					SdkGetMissileCaster(Object, NULL, &CachedObject.Missile->CasterID);
					SdkGetMissileStartPosition(Object, &CachedObject.Missile->StartWorldPosition);
					SdkIsMissileAutoAttack(Object, &CachedObject.Missile->AutoAttack);
					SdkGetMissileSpeed(Object, &CachedObject.Missile->Speed);
					SdkGetMissileTarget(Object, &CachedObject.Missile->TargetWorldPosition, NULL, &CachedObject.Missile->TargetID);
					SdkHasMissileCompleted(Object, &CachedObject.Missile->Completed);
					SdkGetMissileStartTime(Object, &CachedObject.Missile->StartTime);
					SdkGetMissileWidth(Object, &CachedObject.Missile->Width);
					SdkGetMissileParentMissile(Object, NULL, &CachedObject.Missile->ParentMissileID);
				}
				else
				{
					CachedObject.Missile = NULL;
				}

				if (SDKSTATUS_SUCCESS(SdkIsObjectParticle(Object)))
				{
					CachedObject.Particle = std::make_shared<ParticleClient>();
					SdkGetParticleOwner(Object, &CachedObject.Particle->Owner);
				}
				else
				{
					CachedObject.Particle = NULL;
				}

				m_ObjectMapping[TypeName].push_back(CachedObject);
				return true;
			}, 
		NULL);
	}

	//
	// At this point, we display the information we've captured from
	// the previously.
	//
	SdkUiText("There are ");
	SdkUiForceOnSameLine();
	SdkUiColoredText(&m_ColorTeal, "%u", NumberOfObjects);
	SdkUiForceOnSameLine();
	SdkUiText(" game objects.");
	
	//
	// We go through each (stale) object in our map.
	//
	for (const auto& ObjectMapping : m_ObjectMapping)
	{
		//
		// Each object is sorted in by object type name.
		//
		bool MappingExpanded = false;
		SdkUiBeginTreeEx(&MappingExpanded, &ObjectMapping, "Type: %s (%zu entries)", ObjectMapping.first.c_str(), ObjectMapping.second.size());

		if (MappingExpanded)
		{
			for (auto& Object : ObjectMapping.second)
			{
				bool ObjectExpanded = false;
				SdkUiBeginTreeEx(&ObjectExpanded, &Object, "Pointer: 0x%p", Object.Pointer);

				if (ObjectExpanded)
				{
					bool GameObjectExpanded = false;
					SdkUiBeginTreeEx(&GameObjectExpanded, Object.Pointer, "Game Object");

					//
					// Each object is at the very least a game object.
					//
					if (GameObjectExpanded)
					{
						SdkUiBulletText("ID: %u", Object.ID);
						SdkUiBulletText("Network ID: %u", Object.NetworkID);
						SdkUiBulletText("Name: %s", Object.Name.c_str());
						SdkUiBulletText("Type: 0x%x (%s, %d)", Object.TypeFlags, Object.TypeName.c_str(), Object.TypeID);
						SdkUiBulletText("Team: %d", Object.TeamID);
						SdkUiBulletText("Position: { %f, %f, %f }", Object.Position.x, Object.Position.y, Object.Position.z);
						SdkUiBulletText("Velocity: { %f, %f, %f }", Object.Velocity.x, Object.Velocity.y, Object.Velocity.z);
						SdkUiBulletText("Acceleration: { %f, %f, %f }", Object.Acceleration.x, Object.Acceleration.y, Object.Acceleration.z);
						SdkUiBulletText("Orientation: { %f, %f, %f }", Object.Orientation.x, Object.Orientation.y, Object.Orientation.z);
						SdkUiBulletText("BBox: { %f, %f, %f }, { %f, %f, %f }", Object.BBox.Min.x, Object.BBox.Min.y, Object.BBox.Min.z, Object.BBox.Max.x, Object.BBox.Max.y, Object.BBox.Max.z);
						SdkUiBulletText("BRadius: %f", Object.BRadius);
						SdkUiBulletText("Visible on screen: %u", Object.VisibleOnScreen);
						SdkUiBulletText("Zombie: %u", Object.Zombie);
						SdkUiBulletText("Dead: %u", Object.Dead);
						SdkUiBulletText("Respawns: %u", Object.Respawns);

						SdkUiEndTree();
					}

					//
					// Some of these objects may be attackable units.
					//
					if (Object.Unit)
					{
						bool AttackableUnitExpanded = false;
						SdkUiBeginTreeEx(&AttackableUnitExpanded, &Object.Unit, "Attackable Unit");

						if (AttackableUnitExpanded)
						{
							SdkUiBulletText("Visible: %u", Object.Unit->Visible);
							SdkUiBulletText("Time of death: %f", Object.Unit->TimeOfDeath);

							PSDK_ABILITY_RESOURCE PrimaryResource = &Object.Unit->AbilityResources[ABILITY_SLOT_PRIMARY];
							if (PrimaryResource->Enabled && PrimaryResource->Type != ABILITY_TYPE_NONE)
							{
								const char* TypeName = NULL;

								//
								// Utilities are safe to use after we've captured the object's 
								// information, even if the object is no longer valid.
								//
								SdkAbilityResourceTypeToString(PrimaryResource->Type, &TypeName);

								SdkUiBulletText("P => %s: %f / %f", TypeName, PrimaryResource->Current, PrimaryResource->Max);
							}

							PSDK_ABILITY_RESOURCE SecondaryResource = &Object.Unit->AbilityResources[ABILITY_SLOT_SECONDARY];
							if (SecondaryResource->Enabled && SecondaryResource->Type != ABILITY_TYPE_NONE)
							{
								const char* TypeName = NULL;
								SdkAbilityResourceTypeToString(SecondaryResource->Type, &TypeName);

								SdkUiBulletText("S => %s: %f / %f", TypeName, SecondaryResource->Current, SecondaryResource->Max);
							}

							SdkUiBulletText("Mouse over: %u", Object.Unit->MouseOver);

							PSDK_HEALTH Health = &Object.Unit->Health;
							SdkUiBulletText("Health: %f / %f", Health->Current, Health->Max);
							SdkUiBulletText("Shields: %f (A) / %f (P) / %f (M)", Health->AllShield, Health->PhysicalShield, Health->MagicalShield);
							SdkUiBulletText("Invulnerable: %u", Object.Unit->Invulnerable);
							SdkUiBulletText("Targetability: %d", Object.Unit->Targetability);

							SdkUiEndTree();
						}
					}

					auto PrintSpellData = [](PSDK_SPELL Spell)
					{
						SdkUiBulletText("Level: %d", Spell->Level);
						SdkUiBulletText("Cast delay: %f", Spell->CastDelay);
						SdkUiBulletText("Total delay: %f", Spell->TotalDelay);
						SdkUiBulletText("Mana cost: %f", Spell->ManaCost);
						SdkUiBulletText("Start time for cast: %f", Spell->StartTimeForCast);
						SdkUiBulletText("Total cooldown: %f", Spell->TotalCooldown);
						SdkUiBulletText("Base cooldown: %f", Spell->BaseCooldown);
						SdkUiBulletText("Cooldown expires: %f", Spell->CooldownExpires);
						SdkUiBulletText("CDR: %f", Spell->CooldownReductionPercent);
						SdkUiBulletText("Ammo: %d / %d (%d)", Spell->CurrentAmmo, Spell->MaxAmmo, Spell->AmmoUsage);
						SdkUiBulletText("Base ammo recharge time: %f", Spell->BaseAmmoRechargeTime);
						SdkUiBulletText("Time for ammo recharge: %f", Spell->TimeForNextAmmoRecharge);
						SdkUiBulletText("Total ammo recharge time: %f", Spell->TotalAmmoRechargeTime);
						SdkUiBulletText("Toggle state: %d", Spell->ToggleState);
						SdkUiBulletText("Channel duration: %f", Spell->ChannelDuration);
						SdkUiBulletText("Cast range: %f", Spell->CastRange);
						SdkUiBulletText("Primary cast radius: %f", Spell->PrimaryCastRadius);
						SdkUiBulletText("Secondary cast radius: %f", Spell->SecondaryCastRadius);
						SdkUiBulletText("Spell damage ratio: %f", Spell->SpellDamageRatio);
						SdkUiBulletText("Physical damage ratio: %f", Spell->PhysicalDamageRatio);

						bool SpellEffectsExpanded = false;
						SdkUiBeginTreeEx(&SpellEffectsExpanded, &Spell->SpellEffects, "Spell effects");

						if (SpellEffectsExpanded)
						{
							for (size_t i = 0; i < RTL_NUMBER_OF(Spell->SpellEffects); ++i)
							{
								if (!Spell->SpellEffects[i].Valid)
									break;

								SdkUiBulletText("%s: %f", Spell->SpellEffects[i].Name, Spell->SpellEffects[i].Value);
							}

							SdkUiEndTree();
						}

						SdkUiBulletText("Missile speed: %f", Spell->MissileSpeed);
						SdkUiBulletText("Flags: 0x%x", Spell->Flags);
						SdkUiBulletText("Affects flags: 0x%x / 0x%x", Spell->AffectsFlags, Spell->AffectsFlags2);
						SdkUiBulletText("Cast type: %d", Spell->CastType);
						SdkUiBulletText("Charge update interval: %f", Spell->ChargeUpdateInterval);
						SdkUiBulletText("Cast frame: %f", Spell->CastFrame);
						SdkUiBulletText("Targetting type: %d", Spell->TargettingType);
						SdkUiBulletText("Cone angle: %f", Spell->CastConeAngle);
						SdkUiBulletText("Cone distance: %f", Spell->CastConeDistance);
						SdkUiBulletText("Line width: %f", Spell->LineWidth);
						SdkUiBulletText("Line cast length: %d", Spell->LineCastLength);
					};

					//
					// Some of these objects may be artificially intelligent.
					//
					if (Object.AI)
					{
						bool AIBaseClientExpanded = false;
						SdkUiBeginTreeEx(&AIBaseClientExpanded, &Object.AI, "AI Base");

						if (AIBaseClientExpanded)
						{
							SdkUiBulletText("Name: %s", Object.AI->Name.c_str());
							SdkUiBulletText("Skin: %d", Object.AI->SkinID);
							SdkUiBulletText("Player controlled: %u", Object.AI->PlayerControlled);

							bool AIBaseClientStateExpanded = false;
							SdkUiBeginTreeEx(&AIBaseClientStateExpanded, &Object.AI->CanAttack, "State");

							if (AIBaseClientStateExpanded)
							{
								SdkUiBulletText("Can attack? %u", Object.AI->CanAttack);
								SdkUiBulletText("Can crit? %u", Object.AI->CanCrit);
								SdkUiBulletText("Can cast? %u", Object.AI->CanCast);
								SdkUiBulletText("Can move? %u", Object.AI->CanMove);
								SdkUiBulletText("Stealthed: %u", Object.AI->Stealthed);
								SdkUiBulletText("Taunted: %u", Object.AI->Taunted);
								SdkUiBulletText("Feared: %u", Object.AI->Feared);
								SdkUiBulletText("Fleeing: %u", Object.AI->Fleeing);
								SdkUiBulletText("Suppressed: %u", Object.AI->Suppressed);
								SdkUiBulletText("Near sighted: %u", Object.AI->NearSighted);
								SdkUiBulletText("Ghosted: %u", Object.AI->Ghosted);
								SdkUiBulletText("Charmed: %u", Object.AI->Charmed);
								SdkUiBulletText("Slowed: %u", Object.AI->Slowed);
								SdkUiBulletText("Selectable: %u", Object.AI->Selectable);
								SdkUiBulletText("Crit immune: %u", Object.AI->CritImmune);
								SdkUiBulletText("Grounded: %u", Object.AI->Grounded);
								SdkUiBulletText("Obscured: %u", Object.AI->Obscured);
								SdkUiBulletText("Killable: %u", Object.AI->Killable);

								SdkUiEndTree();
							}

							bool AIBaseClientStatsExpanded = false;
							SdkUiBeginTreeEx(&AIBaseClientStatsExpanded, &Object.AI->Armor, "Stats");

							if (AIBaseClientStatsExpanded)
							{
								SdkUiBulletText("Armor: %f", Object.AI->Armor);
								SdkUiBulletText("Bonus armor: %f", Object.AI->BonusArmor);
								SdkUiBulletText("Lethality: %f", Object.AI->Lethality);
								SdkUiBulletText("Armor penetration: %f / %f", Object.AI->FlatArmorPenetration, Object.AI->PercentArmorPenetration);
								SdkUiBulletText("Bonus armor penetration: %f", Object.AI->BonusArmorPenetration);
								SdkUiBulletText("Attack speed: %f", Object.AI->AttackSpeed);
								SdkUiBulletText("Ability power: %f", Object.AI->AbilityPower);
								SdkUiBulletText("Attack damage: %f", Object.AI->AttackDamage);
								SdkUiBulletText("Attack range: %f", Object.AI->AttackRange);
								SdkUiBulletText("Magic resist: %f", Object.AI->MagicResist);
								SdkUiBulletText("Bonus magic resist: %f", Object.AI->BonusMagicResist);
								SdkUiBulletText("Tenacity: %f", Object.AI->Tenacity);
								SdkUiBulletText("Movement speed: %f", Object.AI->MovementSpeed);
								SdkUiBulletText("Crit chance: %f", Object.AI->CritChance);
								SdkUiBulletText("Crit damage multiplier: %f", Object.AI->CritDamageMultiplier);
								SdkUiBulletText("Dodge chance: %f", Object.AI->DodgeChance);
								SdkUiBulletText("Health regen: %f", Object.AI->HealthRegen);
								SdkUiBulletText("Magic penetration: %f / %f", Object.AI->FlatMagicPenetration, Object.AI->PercentMagicPenetration);
								SdkUiBulletText("Life steal: %f", Object.AI->LifeSteal);
								SdkUiBulletText("Spell vamp: %f", Object.AI->SpellVamp);
								SdkUiBulletText("Resource regen: %f / %f", Object.AI->AbilityResourceRegens[ABILITY_SLOT_PRIMARY], Object.AI->AbilityResourceRegens[ABILITY_SLOT_SECONDARY]);
								SdkUiBulletText("Cooldown reduction: %f", Object.AI->CooldownReduction);

								SdkUiEndTree();
							}

							SdkUiBulletText("Bot: %u", Object.AI->Bot);
							SdkUiBulletText("Gold: %f / %f", Object.AI->CurrentGold, Object.AI->TotalGold);

							bool AIBaseClientBuffsExpanded = false;
							SdkUiBeginTreeEx(&AIBaseClientBuffsExpanded, &Object.AI->Buffs, "Buffs");

							if (AIBaseClientBuffsExpanded)
							{
								for (auto& Buff : Object.AI->Buffs)
								{
									if (Buff.Type == BUFF_TYPE_INVALID)
										continue;

									bool AIBaseClientBuffExpanded = false;
									SdkUiBeginTreeEx(&AIBaseClientBuffExpanded, &Buff, "%s", Buff.Name.c_str());

									if (AIBaseClientBuffExpanded)
									{
										const char* TypeName = NULL;

										//
										// Utilities are safe to use after we've captured the object's 
										// information, even if the object is no longer valid.
										//
										SdkBuffTypeToString(Buff.Type, &TypeName);

										SdkUiBulletText("Type: %u (%s)", Buff.Type, TypeName);
										SdkUiBulletText("Start time: %f", Buff.StartTime);
										SdkUiBulletText("End time: %f", Buff.EndTime);
										SdkUiBulletText("Caster: 0x%p (%u)", Buff.CasterObject, Buff.CasterID);
										SdkUiBulletText("Stacks: %d", Buff.Stacks);

										if (Buff.HasCount)
											SdkUiBulletText("Count: %d", Buff.Count);

										PrintSpellData(&Buff.Spell);

										SdkUiEndTree();
									}
								}

								SdkUiEndTree();
							}

							SdkUiBulletText("Combat type: %d", Object.AI->CombatType);

							bool AIBaseClientActiveSpell = false;
							SdkUiBeginTreeEx(&AIBaseClientActiveSpell, &Object.AI->ActiveSpell, "Active spell");
							if (AIBaseClientActiveSpell)
							{
								SdkUiBulletText("Valid: %u", Object.AI->ActiveSpell.Valid);

								if (Object.AI->ActiveSpell.Valid)
								{
									PrintSpellData(&Object.AI->ActiveSpell.SpellCast.Spell);

									SdkUiBulletText("Auto attack: %u", Object.AI->ActiveSpell.SpellCast.IsAutoAttack);
									SdkUiBulletText("Target: 0x%p", Object.AI->ActiveSpell.SpellCast.TargetObject);
									SdkUiBulletText("Start position: { %f, %f, %f }", Object.AI->ActiveSpell.SpellCast.StartPosition.x, Object.AI->ActiveSpell.SpellCast.StartPosition.y, Object.AI->ActiveSpell.SpellCast.StartPosition.z);
									SdkUiBulletText("End position: { %f, %f, %f }", Object.AI->ActiveSpell.SpellCast.EndPosition.x, Object.AI->ActiveSpell.SpellCast.EndPosition.y, Object.AI->ActiveSpell.SpellCast.EndPosition.z);
									SdkUiBulletText("Start time: %f", Object.AI->ActiveSpell.SpellCast.StartTime);
									SdkUiBulletText("End time: %f", Object.AI->ActiveSpell.EndTime);
									SdkUiBulletText("Particle: 0x%p", Object.AI->ActiveSpell.ParticleObject);
									SdkUiBulletText("Spell was cast: %u", Object.AI->ActiveSpell.SpellWasCast);
									SdkUiBulletText("Is charging: %u", Object.AI->ActiveSpell.IsCharging);
									SdkUiBulletText("Is channeling: %u", Object.AI->ActiveSpell.IsChanneling);
									SdkUiBulletText("Channel end time: %f", Object.AI->ActiveSpell.ChannelEndTime);
									SdkUiBulletText("Is stopped: %u", Object.AI->ActiveSpell.IsStopped);
								}

								SdkUiEndTree();
							}

							bool AIBaseClientSpellsExpanded = false;
							SdkUiBeginTreeEx(&AIBaseClientSpellsExpanded, &Object.AI->SpellsEx, "Spells");

							if (AIBaseClientSpellsExpanded)
							{
								for (auto& SpellEx : Object.AI->SpellsEx)
								{
									bool AIBaseClientSpellExpanded = false;
									SdkUiBeginTreeEx(&AIBaseClientSpellExpanded, &SpellEx, "%u: %s", SpellEx.Spell.Slot, SpellEx.Name.c_str());

									if (AIBaseClientSpellExpanded)
									{
										SdkUiBulletText("Castable: %u", SpellEx.Castable);
										SdkUiBulletText("Script name: %s", SpellEx.ScriptName.c_str());
										SdkUiBulletText("Alternate name: %s", SpellEx.AlternateName.c_str());
										SdkUiBulletText("Display name: %s", SpellEx.DisplayName.c_str());
										SdkUiBulletText("Description: %s", SpellEx.Description.c_str());
										SdkUiBulletText("Animation name: %s", SpellEx.AnimationName.c_str());
										SdkUiBulletText("Missile effect name: %s", SpellEx.MissileEffectName.c_str());

										PrintSpellData(&SpellEx.Spell);

										SdkUiEndTree();
									}

								}

								SdkUiEndTree();
							}

							bool AIBaseClientBasicAttackExpanded = false;
							SdkUiBeginTreeEx(&AIBaseClientBasicAttackExpanded, &Object.AI->BasicAttack, "Basic Attack");

							if (AIBaseClientBasicAttackExpanded)
							{
								PrintSpellData(&Object.AI->BasicAttack);

								SdkUiEndTree();
							}

							SdkUiBulletText("Death duration: %f", Object.AI->DeathDuration);
							SdkUiBulletText("Attacking: %u", Object.AI->Attacking);
							SdkUiBulletText("Moving: %u", Object.AI->Moving);

							bool AIBaseClientNavigationExpanded = false;
							SdkUiBeginTreeEx(&AIBaseClientNavigationExpanded, &Object.AI->NavStartWorldPosition, "Navigation");
							if (AIBaseClientNavigationExpanded)
							{
								SdkUiBulletText("Start position: { %f, %f, %f }", Object.AI->NavStartWorldPosition.x, Object.AI->NavStartWorldPosition.y, Object.AI->NavStartWorldPosition.z);
								SdkUiBulletText("Destination: { %f, %f, %f }", Object.AI->NavEndWorldPosition.x, Object.AI->NavEndWorldPosition.y, Object.AI->NavEndWorldPosition.z);
								SdkUiBulletText("Number of waypoints: %zu", Object.AI->NavWaypoints.size());

								for (size_t i = 0; i < Object.AI->NavWaypoints.size(); ++i)
								{
									PSDKVECTOR Waypoint = &Object.AI->NavWaypoints[i];

									//
									// Next waypoint we are moving to gets a special color.
									//

									if (i == Object.AI->NavNextWaypoint)
									{
										SdkUiBulletPoint();
										SdkUiColoredText(&m_ColorTeal, "Waypoint %zu: { %f, %f, %f }", i, Waypoint->x, Waypoint->y, Waypoint->z);
									}
									else
									{
										SdkUiBulletText("Waypoint %zu: { %f, %f, %f }", i, Waypoint->x, Waypoint->y, Waypoint->z);
									}
								}

								SdkUiBulletText("Velocity: { %f, %f, %f }", Object.AI->NavVelocity.x, Object.AI->NavVelocity.y, Object.AI->NavVelocity.z);
								SdkUiBulletText("Dashing: %u", Object.AI->Dashing);
								SdkUiBulletText("Dash speed: %f", Object.AI->DashSpeed);
								SdkUiBulletText("Dash gravity: %f", Object.AI->DashGravity);

								SdkUiEndTree();
							}

							SdkUiBulletText("Casting: %u", Object.AI->Casting);
							SdkUiBulletText("Enemy: %u", Object.AI->EnemyID);
							SdkUiBulletText("Attack delay: %f", Object.AI->AttackDelay);
							SdkUiBulletText("Attack cast delay: %f", Object.AI->AttackCastDelay);
							SdkUiBulletText("Base attack delay: %f", Object.AI->BaseAttackDelay);
							SdkUiBulletText("Base attack damage: %f", Object.AI->BaseAttackDamage);
							SdkUiBulletText("Bonus attack damage: %f", Object.AI->BonusAttackDamage);
							SdkUiBulletText("Health bar world position: { %f, %f, %f }", Object.AI->HealthbarWorldPosition.x, Object.AI->HealthbarWorldPosition.y, Object.AI->HealthbarWorldPosition.z);
							SdkUiBulletText("Health bar screen position: { %f, %f }", Object.AI->HealthbarScreenPosition.x, Object.AI->HealthbarScreenPosition.y);
							SdkUiBulletText("Facing direction: { %f, %f, %f }", Object.AI->FacingDirection.x, Object.AI->FacingDirection.y, Object.AI->FacingDirection.z);
							SdkUiBulletText("Server position: { %f, %f, %f }", Object.AI->ServerPosition.x, Object.AI->ServerPosition.y, Object.AI->ServerPosition.z);

							SdkUiEndTree();
						}
					}

					// 
					// Some of these objects may be heroes.
					//
					if (Object.Hero)
					{
						bool AIHeroClientExpanded = false;
						SdkUiBeginTreeEx(&AIHeroClientExpanded, &Object.Hero, "AI Hero");

						if (AIHeroClientExpanded)
						{
							SdkUiBulletText("Experience: %f", Object.Hero->Experience);
							SdkUiBulletText("Level: %d", Object.Hero->Level);
							SdkUiBulletText("Neutral kills: %d", Object.Hero->NeutralKills);

							bool AIHeroClientItemsExpanded = false;
							SdkUiBeginTreeEx(&AIHeroClientItemsExpanded, &Object.Hero->ItemsEx, "Items");

							if (AIHeroClientItemsExpanded)
							{
								for (auto& ItemEx : Object.Hero->ItemsEx)
								{
									bool AIHeroClientItemExpanded = false;
									SdkUiBeginTreeEx(&AIHeroClientItemExpanded, &ItemEx, "%u: %s", ItemEx.Item.Slot, ItemEx.DisplayName.c_str());

									if (AIHeroClientItemExpanded)
									{
										SdkUiBulletText("Count: %u", ItemEx.Item.Count);
										SdkUiBulletText("Purchased: %f", ItemEx.Item.Purchased);
										SdkUiBulletText("Clickable: %u", ItemEx.Item.Clickable);
										SdkUiBulletText("Charges: %u", ItemEx.Item.Charges);
										SdkUiBulletText("Required level: %d", ItemEx.Item.RequiredLevel);
										SdkUiBulletText("Item ID: %d", ItemEx.Item.ItemID);
										SdkUiBulletText("Maximum stacks: %d", ItemEx.Item.MaximumStacks);
										SdkUiBulletText("Price: %d", ItemEx.Item.Price);
										SdkUiBulletText("Consumable: %u", ItemEx.Item.Consumable);
										SdkUiBulletText("Consumed on purchase: %u", ItemEx.Item.ConsumedOnPurchased);
										SdkUiBulletText("Resellable: %u", ItemEx.Item.Resellable);
										SdkUiBulletText("Flat CDR: %f", ItemEx.Item.FlatCDR);
										SdkUiBulletText("Percent CDR: %f", ItemEx.Item.PercentCDR);
										SdkUiBulletText("Flat HP: %f", ItemEx.Item.FlatHP);
										SdkUiBulletText("Percent HP: %f", ItemEx.Item.PercentHP);
										SdkUiBulletText("Flat HP regen: %f", ItemEx.Item.FlatHPRegen);
										SdkUiBulletText("Percent HP regen: %f", ItemEx.Item.PercentHPRegen);
										SdkUiBulletText("Percent base HP regen: %f", ItemEx.Item.PercentBaseHPRegen);
										SdkUiBulletText("Percent tenacity: %f", ItemEx.Item.PercentTenacity);
										SdkUiBulletText("Percent slow resist: %f", ItemEx.Item.PercentSlowResistance);
										SdkUiBulletText("Flat movement speed: %f", ItemEx.Item.FlatMovementSpeed);
										SdkUiBulletText("Percent movement speed: %f", ItemEx.Item.PercentMovementSpeed);
										SdkUiBulletText("Percent multiplicative movement speed: %f", ItemEx.Item.PercentMultMovementSpeed);
										SdkUiBulletText("Flat armor: %f", ItemEx.Item.FlatArmor);
										SdkUiBulletText("Percent armor: %f", ItemEx.Item.PercentArmor);
										SdkUiBulletText("Flat armor penetration: %f", ItemEx.Item.FlatArmorPen);
										SdkUiBulletText("Percent armor penetration: %f", ItemEx.Item.PercentArmorPen);
										SdkUiBulletText("Percent bonus armor penetration: %f", ItemEx.Item.PercentBonusArmorPen);
										SdkUiBulletText("Flat magic penetration: %f", ItemEx.Item.FlatMagicPen);
										SdkUiBulletText("Percent magic penetration: %f", ItemEx.Item.PercentMagicPen);
										SdkUiBulletText("Percent bonus magic penetration: %f", ItemEx.Item.PercentBonusMagicPen);
										SdkUiBulletText("Flat magic resist: %f", ItemEx.Item.FlatMagicResist);
										SdkUiBulletText("Percent magic resist: %f", ItemEx.Item.PercentMagicResist);
										SdkUiBulletText("Flat dodge: %f", ItemEx.Item.FlatDodge);
										SdkUiBulletText("Flat crit chance: %f", ItemEx.Item.FlatCritChance);
										SdkUiBulletText("Flat miss change: %f", ItemEx.Item.FlatMissChance);
										SdkUiBulletText("Flat crit damage: %f", ItemEx.Item.FlatCritDamage);
										SdkUiBulletText("Percent crit damage: %f", ItemEx.Item.PercentCritDamage);
										SdkUiBulletText("Flat physical damage: %f", ItemEx.Item.FlatPhysicalDamage);
										SdkUiBulletText("Percent physical damage: %f", ItemEx.Item.PercentPhysicalDamage);
										SdkUiBulletText("Flat magic damage: %f", ItemEx.Item.FlatMagicDamage);
										SdkUiBulletText("Percent magic damage: %f", ItemEx.Item.PercentMagicDamage);
										SdkUiBulletText("Percent EXP bonus: %f", ItemEx.Item.PercentExpBonus);
										SdkUiBulletText("Flat attack range: %f", ItemEx.Item.FlatAttackRange);
										SdkUiBulletText("Percent attack range: %f", ItemEx.Item.PercentAttackRange);
										SdkUiBulletText("Flat cast range: %f", ItemEx.Item.FlatCastRange);
										SdkUiBulletText("Percent cast range: %f", ItemEx.Item.PercentCastRange);
										SdkUiBulletText("Percent attack speed: %f", ItemEx.Item.PercentAttackSpeed);
										SdkUiBulletText("Percent multiplicative attack speed: %f", ItemEx.Item.PercentMultAttackSpeed);
										SdkUiBulletText("Percent healing: %f", ItemEx.Item.PercentHealing);
										SdkUiBulletText("Percent lifesteal: %f", ItemEx.Item.PercentLifeSteal);
										SdkUiBulletText("Percent spell vamp: %f", ItemEx.Item.PercentSpellVamp);

										for (size_t i = 0; i < RTL_NUMBER_OF(ItemEx.Item.AbilityResource); ++i)
										{
											const char* TypeString;
											SdkAbilityResourceTypeToString((uint8_t)i, &TypeString);

											bool AIHeroClientARExpanded = false;
											SdkUiBeginTreeEx(&AIHeroClientARExpanded, &ItemEx.Item.AbilityResource[i], "%u: %s", i, TypeString);

											if (AIHeroClientARExpanded)
											{
												PSDK_ITEM_ABILITY_RESOURCE ItemAbilityResource = (PSDK_ITEM_ABILITY_RESOURCE)&ItemEx.Item.AbilityResource[i];

												SdkUiBulletText("Flat: %f", ItemAbilityResource->FlatAR);
												SdkUiBulletText("Percent: %f", ItemAbilityResource->PercentAR);
												SdkUiBulletText("Flat regen: %f", ItemAbilityResource->FlatARRegen);
												SdkUiBulletText("Percent regen: %f", ItemAbilityResource->PercentARRegen);
												SdkUiBulletText("Percent base regen: %f", ItemAbilityResource->PercentBaseARRegen);
												SdkUiBulletText("Flat/level: %f", ItemAbilityResource->FlatARPerLevel);
												SdkUiBulletText("Flat regen/level: %f", ItemAbilityResource->FlatARRegenPerLevel);

												SdkUiEndTree();
											}
										}

										SdkUiBulletText("Sell back percent: %f", ItemEx.Item.SellBackPercent);

										SdkUiEndTree();
									}
								}

								SdkUiEndTree();
							}

							bool AIHeroClientPerksExpanded = false;
							SdkUiBeginTreeEx(&AIHeroClientPerksExpanded, &Object.Hero->Perks, "Perks");

							if (AIHeroClientPerksExpanded)
							{
								for (auto& Perk : Object.Hero->Perks)
									SdkUiBulletText("%s: %u", Perk.Name.c_str(), Perk.ID);

								SdkUiEndTree();
							}

							SdkUiEndTree();
						}
					}

					// 
					// Some of these objects may be minions.
					// 
					if (Object.Minion)
					{
						bool AIMinionClientExpanded = false;
						SdkUiBeginTreeEx(&AIMinionClientExpanded, &Object.Minion, "AI Minion");

						if (AIMinionClientExpanded)
						{
							SdkUiBulletText("Spawn time: %f", Object.Minion->SpawnTime);
							SdkUiBulletText("Camp number: %d", Object.Minion->CampNumber);
							SdkUiBulletText("Lane minion: %u", Object.Minion->LaneMinion);
							SdkUiBulletText("Type: %u", Object.Minion->Type);
							SdkUiBulletText("Level: %d", Object.Minion->Level);
							SdkUiBulletText("Ward: %u", Object.Minion->Ward);

							SdkUiEndTree();
						}
					}

					//
					// Some of these objects may be turrets.
					//
					if (Object.Turret)
					{
						bool AITurretClientExpanded = false;
						SdkUiBeginTreeEx(&AITurretClientExpanded, &Object.Turret, "AI Turret");

						if (AITurretClientExpanded)
						{
							SdkUiBulletText("Position: %d", Object.Turret->Position);
							SdkUiBulletText("Lane: %d", Object.Turret->Lane);

							SdkUiEndTree();
						}
					}

					//
					// Some of these objects may be spell missiles.
					//
					if (Object.Missile)
					{
						bool MissileClientExpanded = false;
						SdkUiBeginTreeEx(&MissileClientExpanded, &Object.Missile, "Missile");

						if (MissileClientExpanded)
						{
							bool MissileClientSpellExpanded = false;
							SdkUiBeginTreeEx(&MissileClientSpellExpanded, &Object.Missile->Spell, "Spell");

							if (MissileClientSpellExpanded)
							{
								PrintSpellData(&Object.Missile->Spell);

								SdkUiEndTree();
							}

							SdkUiBulletText("Caster ID: %u", Object.Missile->CasterID);
							SdkUiBulletText("Start world position: { %f, %f, %f }", Object.Missile->StartWorldPosition.x, Object.Missile->StartWorldPosition.y, Object.Missile->StartWorldPosition.z);
							SdkUiBulletText("Auto attack: %u", Object.Missile->AutoAttack);
							SdkUiBulletText("Speed: %f", Object.Missile->Speed);
							SdkUiBulletText("Target world position: { %f, %f, %f }", Object.Missile->TargetWorldPosition.x, Object.Missile->TargetWorldPosition.y, Object.Missile->TargetWorldPosition.z);
							SdkUiBulletText("Target ID: %u", Object.Missile->TargetID);
							SdkUiBulletText("Completed: %u", Object.Missile->Completed);
							SdkUiBulletText("Start time: %f", Object.Missile->StartTime);
							SdkUiBulletText("Width: %f", Object.Missile->Width);
							SdkUiBulletText("Parent missile ID: %u", Object.Missile->ParentMissileID);

							SdkUiEndTree();
						}
					}

					//
					// Some of these objects may be particles.
					//
					if (Object.Particle)
					{
						bool ParticleClientExpanded = false;
						SdkUiBeginTreeEx(&ParticleClientExpanded, &Object.Particle, "Particle");

						if (ParticleClientExpanded)
						{
							SdkUiBulletText("Owner: %p", Object.Particle->Owner);

							SdkUiEndTree();
						}
					}

					SdkUiEndTree();
				}
			}

			SdkUiEndTree();
		}
	}
		
	//
	// Signal that we're done displaying this window.
	//
	SdkUiEndWindow();
}