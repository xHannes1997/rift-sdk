/*++

Module Name:

   coords.cpp

Abstract:

	Demonstrates how to use the Rift SDK to convert screen coordinates
	(x, y) to world coordinates (x, y, z) and vice-versa. 

--*/

#include "stdafx.h"
#include "coords.h"
#include "debug.h"

//
// Screen (x, y) coordinates, aka a POINT.
//
SDKPOINT Coordinates::m_CurrentScreenCoordinates;

//
// World (x, y, z), in-game, coordinates, aka a VECTOR.
//
SDKVECTOR Coordinates::m_CurrentWorldCoordinates;

//
// Whether the window is currently shown in the overlay.
//
bool Coordinates::ShowWindow = false;

void Coordinates::Initialize(
	void
)
/*++

Routine Description:

	Initializes class specific data structures.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// Initialize everything to zero: (0, 0) and (0, 0, 0).
	//
	memset(&m_CurrentScreenCoordinates, 0, sizeof(m_CurrentScreenCoordinates));
	memset(&m_CurrentWorldCoordinates, 0, sizeof(m_CurrentWorldCoordinates));
}

void Coordinates::Draw(
	void
)
/*++

Routine Description:

	This function is invoked each frame while the overlay is present
	and the target window is being shown.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// Get the previous window's location and size.
	//
	SDKPOINT Position, Size;
	SdkUiGetWindowDim(&Position, &Size);

	// 
	// Set this new window to display directly to the right of the 
	// previous window.
	//
	Position.x += (Size.x + 10.f);
	SdkUiSetNextWindowPos(&Position);

	//
	// Create the new window.
	//
	bool Collapsed = false;
	SdkUiBeginWindow("Coordinates", &Coordinates::ShowWindow, &Collapsed);
	if (!Collapsed)
	{
		//
		// Early out here if the window is partially non-visible.
		//
		SdkUiEndWindow();
		return;
	}

	//
	// Screen (x, y) coordinates.
	// 
	SdkUiInputPoint("Screen", &m_CurrentScreenCoordinates, 0, NULL); 

	//
	// Display the button on the same line (instead of below) as the
	// screen coordinates.
	//
	SdkUiForceOnSameLine();

	bool ToWorld = false;
	SdkUiSmallButton("Convert##1", &ToWorld);
	if (ToWorld)
	{
		//
		// Convert the screen coordinates (x, y) to in-game (world) 
		// coordinates (x, y, z).
		//
		SdkScreenToWorld(&m_CurrentScreenCoordinates, &m_CurrentWorldCoordinates);
	}

	//
	// World (x, y, z) coordinates.
	//
	SdkUiInputVector("World", &m_CurrentWorldCoordinates, 3, NULL);
	SdkUiForceOnSameLine();

	bool ToScreen = false;
	SdkUiSmallButton("Convert##2", &ToScreen);
	if (ToScreen)
	{
		//
		// Convert the world coordinates (x, y, z) to screen (x, y)
		// coordinates.
		//
		SdkWorldToScreen(&m_CurrentWorldCoordinates, &m_CurrentScreenCoordinates);
	}

	//
	// If checked, the mouse's screen position (x, y) will be tracked
	// and converted to world position (x, y, z) automatically.
	//
	// NOTE: This is marked as 'static' so it's effectively a global
	// variable and therefore won't be reset to 'false' each frame.
	//
	static bool TrackMouse = false;
	SdkUiCheckbox("Track Mouse", &TrackMouse, NULL);

	//
	// Clicking the previous buttons or pressing the F10 key will 
	// stop the mouse from being tracked. 
	//
	if (GetAsyncKeyState(VK_F10) || ToWorld || ToScreen)
	{
		TrackMouse = false;
	}

	if (TrackMouse)
	{
		SdkUiText("Press 'F10' to stop tracking.");

		//
		// Get the mouse cursor's position.
		//
		SdkGetMouseScreenPosition(&m_CurrentScreenCoordinates);

		// 
		// Convert these screen coordinates (x, y) to world 
		// coordinates (x, y, z).
		// 
		SdkScreenToWorld(&m_CurrentScreenCoordinates, &m_CurrentWorldCoordinates);
	}

	//
	// Signal that we're done displaying this window.
	//
	SdkUiEndWindow();
}