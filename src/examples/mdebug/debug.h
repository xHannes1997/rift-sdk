/*++

Module Name:

   debug.h

Abstract:

	This module showcases how to use the Rift API to:
		* access the local player object,
		* render hack menus to display information and take in user
		  input,
		* display in-game visuals,
		* convert world to screen coordinates and vice-versa,
		* iterate over game objects such as attackable units,
		  heroes, minions, turrets, and spell missiles,
		* control local player (and pet) input to issue movement,
		  attack, spell cast, emote, and chat commands

	This is considered an 'all-in-one' module for Rift developers to
	use in troubleshooting and creating their own derivatives.

--*/


#pragma once

///
/// Globals.
///

extern PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

extern void* _g_LocalPlayer;

///
/// Function definitions.
///

void
__cdecl
DrawOverlayScene(
	_In_ void* UserData
);

void
__cdecl
DrawGameScene(
	_In_ void* UserData
);