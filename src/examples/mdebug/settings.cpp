/*++

Module Name:

   settings.cpp

Abstract:

	Illustrates how to use the Rift SDK to store and retrieve persistent
	settings.

--*/

#include "stdafx.h"
#include "settings.h"
#include "debug.h"

//
// Whether the window is currently shown in the overlay.
//
bool Settings::ShowWindow = false;

//
// These values are retrieved dynamically from the module-specific
// configuration store and displayed in the UI.
//
char Settings::m_Text[100];
bool Settings::m_Boolean;
float Settings::m_Float;
int Settings::m_Number;

void Settings::Initialize(
	void
)
/*++

Routine Description:

	Initializes class specific data structures.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// These values are retrieved dynamically from the module-specific
	// configuration store.
	//
	SdkGetSettingString("Text", m_Text, RTL_NUMBER_OF(m_Text), "No saved text.");
	SdkGetSettingBool("Boolean", &m_Boolean, false);
	SdkGetSettingFloat("Float", &m_Float, 0.f);
	SdkGetSettingNumber("Number", &m_Number, 0);
}

void Settings::Draw(
	void
)
/*++

Routine Description:

	This function is invoked each frame while the overlay is present
	and the target window is being shown.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// Get the previous window's location and size.
	//
	SDKPOINT Position, Size;
	SdkUiGetWindowDim(&Position, &Size);

	// 
	// Set this new window to display directly to the right of the 
	// previous window.
	//
	Position.x += (Size.x + 10.f);
	SdkUiSetNextWindowPos(&Position);

	//
	// Create the new window.
	//
	bool Collapsed = false;
	SdkUiBeginWindow("Settings", &Settings::ShowWindow, &Collapsed);
	if (!Collapsed)
	{
		//
		// Early out here if the window is partially non-visible.
		//
		SdkUiEndWindow();
		return;
	}

	//
	// A text message.
	//
	SdkUiInputText("Text", m_Text, RTL_NUMBER_OF(m_Text), 0, NULL);

	//
	// A checkbox: boolean.
	//
	SdkUiCheckbox("Boolean", &m_Boolean, NULL);

	//
	// A floating point UI element.
	//
	SdkUiInputFloat("Float", &m_Float, 3, NULL);

	//
	// An input number UI element.
	//
	SdkUiInputNumber("Number", &m_Number, NULL);

	bool SaveClicked = false;
	SdkUiButton("Save", &SaveClicked);
	if (SaveClicked)
	{
		//
		// Save these new values directly to the module-specific 
		// configuration store.
		//

		SdkSetSettingString("Text", m_Text);
		SdkSetSettingBool("Boolean", m_Boolean);
		SdkSetSettingFloat("Float", m_Float);
		SdkSetSettingNumber("Number", m_Number);
	}

	bool ReloadClicked = false;
	SdkUiButton("Reload", &ReloadClicked);
	if (ReloadClicked)
	{
		//
		// Reload effectively loads the saved values directly from
		// the module-specific configuration store.
		//

		Initialize();
	}

	//
	// Signal that we're done displaying this window.
	//
	SdkUiEndWindow();
}