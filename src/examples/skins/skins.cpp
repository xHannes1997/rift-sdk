/*++

Module Name:

   skins.cpp

Abstract:

    This module showcases how to use the Rift API to create a skin 
	changer by accessing the local player object, retrieving the
	current champion, enumerating available skins for the champion,
	and altering the selected skin based on the user's input. 
	Additionally, creating UI elements within the Rift overlay is 
	demonstrated.

--*/

//
// This lets us use Windows types, structures, and defined API.
//
#include <Windows.h>

//
// C++-specific containers.
//
#include <algorithm>
#include <map>

//
// In order to use the Rift API, we include this.
//
#include "../../sdkapi.h"

//
// This stores a pointer to all the SDK functions we can use. This is 
// intentionally a global so that other functions can use it.
//
PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

//
// The local player object never changes. We can store it once after we
// retrieve it from the SDK.
//
void* _g_LocalPlayer = NULL;

//
// The local player's selected champion.
//
std::string _g_ChampionName;

//
// A collection of skins for a champion.
//
std::map<int, std::string> _g_Skins;

// 
// Stores the state of the selected skin.
//
std::pair<const char*, int> _g_CurrentSelection;

void 
__cdecl 
DrawOverlayScene(
	_In_ void* UserData
)
/*++

Routine Description:

	This function is invoked every frame when the overlay is visible.

Arguments:

	UserData - A pointer to an arbitrary data structure, provided 
		by the user, when the overlay scene was registered.

Return Value:

	None.

--*/
{
	UNREFERENCED_PARAMETER(UserData);

	//
	// Use a combo control (dropdown list).
	//
	bool ComboClicked = false;
	SdkUiBeginCombo("Skin", _g_CurrentSelection.first, &ComboClicked);

	if (ComboClicked)
	{
		//
		// For each skin for this champion, add an entry in the dropdown 
		// list.
		//
		for (auto& Skin : _g_Skins)
		{
			//
			// Add the entry.
			//
			bool ItemSelected = (_g_CurrentSelection.first == Skin.second.c_str());
			bool ItemClicked = false;

			SdkUiAddSelection(Skin.second.c_str(), ItemSelected, &ItemClicked);
			if (ItemClicked)
			{
				//
				// If this statement is entered, that means the selection has 
				// been updated to this new index.
				//
				_g_CurrentSelection.second = Skin.first;
				_g_CurrentSelection.first = Skin.second.c_str();
			}

			if (ItemSelected)
			{
				//
				// Set the focus of the selected entry.
				//
				SdkUiSetItemDefaultFocus();
			}
		}

		//
		// End the combo (dropdown list) control so it can be rendered.
		//
		SdkUiEndCombo();
	}

	//
	// Create a button on the window that the user will press to apply 
	// their skin selection.
	//
	bool ApplyClicked = false;
	SdkUiButton("Apply", &ApplyClicked);
	if (ApplyClicked)
	{
		//
		// If the button is pressed, load the newly selected skin by the player.
		//
		SdkSetAISkinID(_g_LocalPlayer, _g_CurrentSelection.second);

		//
		// Save the last skin we've loaded for this champion.
		//
		SdkSetSettingNumber(_g_ChampionName.c_str(), _g_CurrentSelection.second);
	}
}

BOOL 
WINAPI 
DllMain(
	_In_ HINSTANCE hinstDLL, 
	_In_ DWORD fdwReason, 
	_In_ LPVOID lpvReserved
)
/*++

Routine Description:

	This function is called when the SDK loads the module. Consider
	this your module entry point.

Arguments:
	
	hinstDLL - The same value as lpvReserved when loaded by the SDK.

	fdwReason - Exclusively DLL_PROCESS_ATTACH (1) when loaded by the
		SDK.

	lpvReserved - The same value as hinstDLL when loaded by the SDK.
		Contains a pointer to the SDK_CONTEXT structure that can be 
		retrieved via the SDK_EXTRACT_CONTEXT macro. This structure
		contains all the necessary function pointers required to 
		interface with the Rift SDK.

Return Value:

	'TRUE' indicates module load success. 'FALSE' indicates module
	load failure.

--*/
{
	UNREFERENCED_PARAMETER(hinstDLL);

	//
	// We're only interested when the DLL attaches to the process.
	//
	if (fdwReason != DLL_PROCESS_ATTACH)
		return TRUE;

	//
	// This macro extracts the pointer to the SDK context from the lpvReserved
	// parameter.
	//
	SDK_EXTRACT_CONTEXT(lpvReserved);
	if (!SDK_CONTEXT_GLOBAL)
		return FALSE;

	//
	// Every module loaded by Rift should have a call to SdkNotifyLoadedModule
	// as soon as possible to ensure that other SDK API can be called.
	//
	if (!SDKSTATUS_SUCCESS(SdkNotifyLoadedModule("Skins", SDK_VERSION)))
	{
		//
		// This routine will fail if a module of the same name is already
		// loaded or if there's a mismatch with the target SDK version.
		//
		return FALSE;
	}

	//
	// In order to use basically any SDK function (aside from 
	// SdkNotifyLoadedModule and SdkRegisterOnLoad), we need to be
	// in the main game thread. 
	//
	// All callbacks are executed in the context of the main game 
	// thread. Calling SdkRegisterOnLoad here ensures that we can 
	// use other SDK API safely as it will execute in the main
	// game thread.
	//
	SdkRegisterOnLoad
	(
		[](void* UserData) -> void
		{
			UNREFERENCED_PARAMETER(UserData);

			//
			// Retrieve the local player object.
			//
			if (!SDKSTATUS_SUCCESS(SdkGetLocalPlayer(&_g_LocalPlayer)) || !_g_LocalPlayer)
			{
				SdkUiConsoleWrite("[error] Could not retrieve the local player unit.\n");
				return;
			}

			//
			// Get the name of the champion the player is playing.
			//
			const char* ChampionName = NULL;
			if (!SDKSTATUS_SUCCESS(SdkGetAIName(_g_LocalPlayer, &ChampionName)) || !ChampionName)
			{
				SdkUiConsoleWrite("[error] Could not retrieve the champion name of the local player unit.\n");
				return;
			}

			//
			// NOTE: This does a duplication, intentionally, to avoid
			// memory allocation issues past the frame in which this
			// data was retrieved.
			//
			_g_ChampionName = ChampionName;

			//
			// Enumerate all skins for that champion.
			//
			SdkEnumChampionSkinNames
			(
				ChampionName,
				[](const char* SkinName, int SkinID, void* UserData) -> bool
				{
					UNREFERENCED_PARAMETER(UserData);

					//
					// Store off the metadata in the skins collection.
					//
					// Add to the collection. We do this once before the overlay 
					// is registered to avoid having to perform this operation 
					// every frame.
					//
					_g_Skins[SkinID] = SkinName;

					//
					// Keep iterating - return 'false' to stop the enumeration 
					// early.
					//
					return true;
				}, NULL
			);

			//
			// Each champion has at least one skin. If enumeration failed, the 
			// collection will have no entries.
			//
			if (!_g_Skins.size())
			{
				SdkUiConsoleWrite("[error] Could not retrieve skin data for champion '%s'.\n", ChampionName);
				return;
			}

			//
			// Get the player's current skin.
			//
			int CurrentSkinID = 0;
			if (!SDKSTATUS_SUCCESS(SdkGetAISkinID(_g_LocalPlayer, &CurrentSkinID)) || !_g_Skins.count(CurrentSkinID))
			{
				SdkUiConsoleWrite("[error] Local player using skin %d which is not in champion data (%zu entries).\n", CurrentSkinID, _g_Skins.size());
				return;
			}

			//
			// Load the last saved data (if it exists).
			//
			SdkGetSettingNumber(_g_ChampionName.c_str(), &_g_CurrentSelection.second, CurrentSkinID);
			if (!_g_Skins.count(_g_CurrentSelection.second))
			{
				SdkUiConsoleWrite("[error] Local player tried to load skin %d which is not in champion data (%zu entries).\n", _g_CurrentSelection.second, _g_Skins.size());
				return;
			}

			//
			// Apply the last skin selection for this champion.
			//
			if (CurrentSkinID != _g_CurrentSelection.second)
				SdkSetAISkinID(_g_LocalPlayer, _g_CurrentSelection.second);

			_g_CurrentSelection.first = _g_Skins[_g_CurrentSelection.second].c_str();

			//
			// When the overlay is being drawn (e.g. hack menu is up), invoke
			// our function.
			//
			SdkRegisterOverlayScene(DrawOverlayScene, NULL);

		}, NULL
	);

	return TRUE;
}