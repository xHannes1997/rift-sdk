/*++

Module Name:

   simple.cpp

Abstract:

    This module showcases how to use the Rift API in a simplistic
	matter by writing a message to the developer console when it 
	is loaded.

--*/

//
// This lets us use Windows types, structures, and defined API.
//
#include <Windows.h>

//
// In order to use the Rift API, we include this.
//
#include "../../sdkapi.h"

//
// This stores a pointer to all the SDK functions we can use. This is 
// intentionally a global so that other functions can use it.
//
PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

BOOL
WINAPI
DllMain(
	_In_ HINSTANCE hinstDLL,
	_In_ DWORD fdwReason,
	_In_ LPVOID lpvReserved
)
/*++

Routine Description:

	This function is called when the SDK loads the module. Consider
	this your module entry point.

Arguments:

	hinstDLL - The same value as lpvReserved when loaded by the SDK.

	fdwReason - Exclusively DLL_PROCESS_ATTACH (1) when loaded by the
		SDK.

	lpvReserved - The same value as hinstDLL when loaded by the SDK.
		Contains a pointer to the SDK_CONTEXT structure that can be
		retrieved via the SDK_EXTRACT_CONTEXT macro. This structure
		contains all the necessary function pointers required to
		interface with the Rift SDK.

Return Value:

	'TRUE' indicates module load success. 'FALSE' indicates module
	load failure.

--*/
{
	//
	// This parameter is unused.
	//
	UNREFERENCED_PARAMETER(hinstDLL);

	//
	// We're only interested when the DLL attaches to the process.
	//
	if (fdwReason != DLL_PROCESS_ATTACH)
		return TRUE;

	//
	// This macro extracts the pointer to the SDK context from the lpvReserved
	// parameter.
	//
	SDK_EXTRACT_CONTEXT(lpvReserved);
	if (!SDK_CONTEXT_GLOBAL)
		return FALSE;

	//
	// Every module loaded by Rift should have a call to SdkNotifyLoadedModule
	// as soon as possible to ensure that other SDK API can be called.
	//
	if (!SDKSTATUS_SUCCESS(SdkNotifyLoadedModule("Simple", SDK_VERSION)))
	{
		//
		// This routine will fail if a module of the same name is already
		// loaded or if there's a mismatch with the target SDK version.
		//
		return FALSE;
	}

	//
	// In order to use basically any SDK function (aside from 
	// SdkNotifyLoadedModule and SdkRegisterOnLoad), we need to be
	// in the main game thread. 
	//
	// All callbacks are executed in the context of the main game 
	// thread. Calling SdkRegisterOnLoad here ensures that we can 
	// use other SDK API safely as it will execute in the main
	// game thread.
	//
	SdkRegisterOnLoad
	(
		[](void* UserData) -> void 
		{
			UNREFERENCED_PARAMETER(UserData);

			//
			// Write a "hello world" message to the developer console. 
			//
			// We use the Windows API (GetCurrentProcessId and GetCurrentThreadId) to show 
			// that it's easy and possible to do!
			//
			SdkUiConsoleWrite("[simple] Hello! Executing in process %u and thread %u.\n", GetCurrentProcessId(), GetCurrentThreadId());

		}, NULL
	);

	return TRUE;
}