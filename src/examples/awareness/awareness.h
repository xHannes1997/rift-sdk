/*++

Module Name:

   awareness.h

Abstract:

	This module showcases how to use the Rift API to create a simple
	awareness plugin:
		* draw the attack range of the local player object,
		* track ally and enemy spell cooldowns,
		* track wards and their vision ranges,
		* highlight tower attack ranges,
		* dynamically save and retrieve persistent settings

--*/

#pragma once

///
/// Function definitions.
///

void 
__cdecl 
DrawOverlayScene(
	_In_ void* UserData
);

void 
__cdecl 
DrawGameScene(
	_In_ void* UserData
);

bool 
__cdecl 
AwObjectLoop(
	_In_ void* Object, 
	_In_ unsigned int NetworkID,
	_In_opt_ void* UserData
);

void 
AwLoadSettings(
	void
);

void
AwSaveSettings(
	void
);