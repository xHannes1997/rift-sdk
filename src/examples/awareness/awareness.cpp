/*++

Module Name:

   awareness.cpp

Abstract:

	This module showcases how to use the Rift API to create a simple
	awareness plugin:
		* draw the attack range of the local player object,
		* track ally and enemy spell cooldowns,
		* track wards and their vision ranges,
		* highlight tower attack ranges,
		* dynamically save and retrieve persistent settings

--*/

//
// This lets us use Windows types, structures, and defined API.
//
#include <Windows.h>

//
// To use mutable string objects.
//
#include <string>
#include <sstream>

//
// C++-specific containers.
//
#include <map>

//
// In order to use the Rift API, we include this.
//
#include "../../sdkapi.h"

//
// Forward declaration of the necessary functions.
//
#include "awareness.h"

//
// This stores a pointer to all the SDK functions we can use. This is 
// intentionally a global so that other functions can use it.
//
PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

//
// Used for drawing a circle with the default direction vector.
//
SDKVECTOR _g_DirectionVector = { 0, 0, 1.f };

//
// Colors in the BGRA format.
//
SDKCOLOR _g_ColorWhite = { 255, 255, 255, 255 };
SDKCOLOR _g_ColorYellow = { 0, 255, 255, 255 };
SDKCOLOR _g_ColorGreen = { 0, 255, 0, 255 };
SDKCOLOR _g_ColorRed = { 0, 0, 255, 255 };
SDKCOLOR _g_ColorPurple = { 128, 0, 128, 255 };

struct
{
	//
	// The local player object never changes. We can store it once after we
	// retrieve it from the SDK.
	//
	void* Object;

	//
	// The player's team.
	//
	int TeamID;

	//
	// Whether or not we should draw the player's attack range.
	//
	bool bDrawAttackRange;
} _g_LocalPlayer;

struct
{
	//
	// Whether or not we should track spell cooldowns.
	//
	bool bTrackCooldowns;

	//
	// Whether or not we should track the local player's cooldowns.
	//
	bool bIncludeLocalPlayer;

	//
	// Whether or not we track D and F.
	//
	bool bIncludeSummonerSpells;

	//
	// Whether we should draw text for spells that are off cooldown. 
	//
	bool bIncludeSpellsOffCooldown;
} _g_Heroes;

struct  
{
	//
	// Whether to highlight minions that we can kill with an auto-attack.
	//
	bool bDrawLastHittable;

	//
	// Whether we should draw the vision range of a ward.
	//
	bool bDrawWardVisionRange;

	//
	// Whether we should ping the drawn ward.
	//
	bool bPingWards;

	//
	// A collection of wards that we've pinged so that we avoid re-pinging
	// them each frame.
	//
	std::map<unsigned int, bool> PingedWards;
} _g_Minions;

struct
{
	//
	// Whether we should draw the attack range of a turret.
	//
	bool bDrawAttackRange;

	//
	// Whether we should draw the attack radius of only enemy turrets.
	//
	bool bDrawOnlyEnemies;
} _g_Turrets;

//
// Short-hand notation for a spell hotkey.
//
char _g_Spells[] = { 'Q', 'W', 'E', 'R', 'D', 'F' };

BOOL 
WINAPI 
DllMain(
	_In_ HINSTANCE hinstDLL, 
	_In_ DWORD fdwReason, 
	_In_ LPVOID lpvReserved
)
/*++

Routine Description:

	This function is called when the SDK loads the module. Consider
	this your module entry point.

Arguments:

	hinstDLL - The same value as lpvReserved when loaded by the SDK.

	fdwReason - Exclusively DLL_PROCESS_ATTACH (1) when loaded by the
		SDK.

	lpvReserved - The same value as hinstDLL when loaded by the SDK.
		Contains a pointer to the SDK_CONTEXT structure that can be
		retrieved via the SDK_EXTRACT_CONTEXT macro. This structure
		contains all the necessary function pointers required to
		interface with the Rift SDK.

Return Value:

	'TRUE' indicates module load success. 'FALSE' indicates module
	load failure.

--*/
{
	UNREFERENCED_PARAMETER(hinstDLL);

	//
	// We're only interested when the DLL attaches to the process.
	//
	if (fdwReason != DLL_PROCESS_ATTACH)
		return TRUE;

	//
	// This macro extracts the pointer to the SDK context from the lpvReserved
	// parameter.
	//
	SDK_EXTRACT_CONTEXT(lpvReserved);
	if (!SDK_CONTEXT_GLOBAL)
		return FALSE;

	//
	// Every module loaded by Rift should have a call to SdkNotifyLoadedModule
	// as soon as possible to ensure that other SDK API can be called.
	//
	if (!SDKSTATUS_SUCCESS(SdkNotifyLoadedModule("Awareness", SDK_VERSION)))
	{
		//
		// This routine will fail if a module of the same name is already
		// loaded or if there's a mismatch with the target SDK version.
		//
		return FALSE;
	}

	//
	// In order to use basically any SDK function (aside from 
	// SdkNotifyLoadedModule and SdkRegisterOnLoad), we need to be
	// in the main game thread. 
	//
	// All callbacks are executed in the context of the main game 
	// thread. Calling SdkRegisterOnLoad here ensures that we can 
	// use other SDK API safely as it will execute in the main
	// game thread.
	//
	SdkRegisterOnLoad
	(
		[](void* UserData) -> void
		{
			UNREFERENCED_PARAMETER(UserData);

			//
			// Retrieve the local player object.
			//
			SdkGetLocalPlayer(&_g_LocalPlayer.Object);

			//
			// Retrieve the team for the local player.
			// 
			SdkGetObjectTeamID(_g_LocalPlayer.Object, &_g_LocalPlayer.TeamID);

			//
			// Load module-specific configuration store for persistent settings.
			//
			AwLoadSettings();

			//
			// When the overlay is being drawn (e.g. hack menu is up), invoke
			// our function.
			//
			SdkRegisterOverlayScene(DrawOverlayScene, NULL);

			//
			// Each frame, the SDK will call into this function where we can 
			// add draw additional visuals before it's dispatched to the GPU
			// to render.
			//
			SdkRegisterGameScene(DrawGameScene, NULL);
		}, NULL
	);

	//
	// The base 'game frame' sits below our extra visuals (the 'game
	// scene'). The 'overlay scene' sits atop both of these, but is
	// only visible when the end user presses the required hotkey.
	//
	return TRUE;
}

void 
AwLoadSettings(
	void
)
/*++

Routine Description:

	Loads module-specific persistent data.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// These values are retrieved dynamically from the module-specific
	// configuration store.
	//

	SdkGetSettingBool("Player_DrawAttackRange", &_g_LocalPlayer.bDrawAttackRange, false);

	SdkGetSettingBool("Minions_DrawLastHittable", &_g_Minions.bDrawLastHittable, true);
	SdkGetSettingBool("Minions_DrawWardVisionRange", &_g_Minions.bDrawWardVisionRange, true);
	SdkGetSettingBool("Minions_PingWards", &_g_Minions.bPingWards, true);

	SdkGetSettingBool("Turrets_DrawAttackRange", &_g_Turrets.bDrawAttackRange, true);
	SdkGetSettingBool("Turrets_DrawOnlyEnemies", &_g_Turrets.bDrawOnlyEnemies, true);

	SdkGetSettingBool("Heroes_TrackCooldowns", &_g_Heroes.bTrackCooldowns, true);
	SdkGetSettingBool("Heroes_IncludeLocalPlayer", &_g_Heroes.bIncludeLocalPlayer, false);
	SdkGetSettingBool("Heroes_IncludeSummonerSpells", &_g_Heroes.bIncludeSummonerSpells, true);
	SdkGetSettingBool("Heroes_IncludeSpellsOffCooldown", &_g_Heroes.bIncludeSpellsOffCooldown, true);
}

void 
AwSaveSettings(
	void
)
/*++

Routine Description:

	Save module-specific persistent data.

Arguments:

	None.

Return Value:

	None.

--*/
{
	//
	// Save these new values directly to the module-specific 
	// configuration store.
	//

	SdkSetSettingBool("Player_DrawAttackRange", _g_LocalPlayer.bDrawAttackRange);

	SdkSetSettingBool("Minions_DrawLastHittable", _g_Minions.bDrawLastHittable);
	SdkSetSettingBool("Minions_DrawWardVisionRange", _g_Minions.bDrawWardVisionRange);
	SdkSetSettingBool("Minions_PingWards", _g_Minions.bPingWards);

	SdkSetSettingBool("Turrets_DrawAttackRange", _g_Turrets.bDrawAttackRange);
	SdkSetSettingBool("Turrets_DrawOnlyEnemies", _g_Turrets.bDrawOnlyEnemies);

	SdkSetSettingBool("Heroes_TrackCooldowns", _g_Heroes.bTrackCooldowns);
	SdkSetSettingBool("Heroes_IncludeLocalPlayer", _g_Heroes.bIncludeLocalPlayer);
	SdkSetSettingBool("Heroes_IncludeSummonerSpells", _g_Heroes.bIncludeSummonerSpells);
	SdkSetSettingBool("Heroes_IncludeSpellsOffCooldown", _g_Heroes.bIncludeSpellsOffCooldown);

}

void
__cdecl
DrawOverlayScene(
	_In_ void* UserData
)
/*++

Routine Description:

	This function is invoked every frame when the overlay is visible.

Arguments:

	UserData - A pointer to an arbitrary data structure, provided
		by the user, when the overlay scene was registered.

Return Value:

	None.

--*/ 
{
	UNREFERENCED_PARAMETER(UserData);

	//
	// Settings for local player awareness.
	//
	SdkUiCheckbox("Draw local player attack range", &_g_LocalPlayer.bDrawAttackRange, NULL);

	//
	// Settings for champion awareness.
	//
	bool bHeroesExpanded = false;
	SdkUiBeginTree("Heroes", &bHeroesExpanded);
	if (bHeroesExpanded)
	{
		SdkUiCheckbox("Track cooldowns", &_g_Heroes.bTrackCooldowns, NULL);

		//
		// Additional options for cooldown tracking.
		//
		if (_g_Heroes.bTrackCooldowns)
		{
			SdkUiCheckbox("Include local player", &_g_Heroes.bIncludeLocalPlayer, NULL);
			SdkUiCheckbox("Include summoner spells", &_g_Heroes.bIncludeSummonerSpells, NULL);
			SdkUiCheckbox("Include spells off cooldown", &_g_Heroes.bIncludeSpellsOffCooldown, NULL);
		}

		SdkUiEndTree();
	}

	//
	// Settings for minion awareness.
	//
	bool bMinionsExpanded = false;
	SdkUiBeginTree("Minions", &bMinionsExpanded);
	if (bMinionsExpanded)
	{
		SdkUiCheckbox("Draw low health (last hittable)", &_g_Minions.bDrawLastHittable, NULL);
		SdkUiCheckbox("Draw ward vision range", &_g_Minions.bDrawWardVisionRange, NULL);

		if (_g_Minions.bDrawWardVisionRange)
		{
			//
			// Should we ping newly spawned wards?
			//
			SdkUiCheckbox("Also ping wards", &_g_Minions.bPingWards, NULL);
		}

		SdkUiEndTree();
	}

	//
	// Settings for tower awareness.
	//
	bool bTurretsExpanded = false;
	SdkUiBeginTree("Turrets", &bTurretsExpanded);
	if (bTurretsExpanded)
	{
		SdkUiCheckbox("Draw ranges", &_g_Turrets.bDrawAttackRange, NULL);
		SdkUiCheckbox("Draw only enemies", &_g_Turrets.bDrawOnlyEnemies, NULL);

		SdkUiEndTree();
	}

	bool SaveClicked = false;
	SdkUiButton("Save settings", &SaveClicked);
	if (SaveClicked)
	{
		//
		// Save these settings.
		//

		AwSaveSettings();
	}

	bool ReloadClicked = false;
	SdkUiButton("Reload settings", &ReloadClicked);
	if (ReloadClicked)
	{
		//
		// Reload effectively loads the saved values directly from
		// the module-specific configuration store.
		//

		AwLoadSettings();
	}
}

void
__cdecl
DrawGameScene(
	_In_ void* UserData
)
/*++

Routine Description:

	This function is invoked every frame.

Arguments:

	UserData - A pointer to an arbitrary data structure, provided
		by the user, when the overlay scene was registered.

Return Value:

	None.

--*/
{
	UNREFERENCED_PARAMETER(UserData);

	//
	// Enumerate game objects.
	//
	SdkEnumGameObjects(AwObjectLoop, NULL);
}

static 
void 
ProcessLocalPlayer(
	_In_ SDKVECTOR& Position
)
/*++

Routine Description:

	This function processes the local player object.

Arguments:

	Position - The location of the object in world coordinates (x, y, z).

Return Value:

	None.

--*/
{
	//
	// Are we drawing the player's attack range?
	//
	if (_g_LocalPlayer.bDrawAttackRange)
	{
		//
		// Use the attack range as the radius of the circle.
		//
		float Radius;
		SdkGetAIAttackRange(_g_LocalPlayer.Object, &Radius);

		float BRadius;
		SdkGetObjectBoundingRadius(_g_LocalPlayer.Object, &BRadius);

		Radius += BRadius;

		//
		// Draw the circle.
		//
		SdkDrawCircle(&Position, Radius, &_g_ColorWhite, 0, &_g_DirectionVector);
	}
}

static 
void 
ProcessMinion(
	_In_ void* Object, 
	_In_ SDKVECTOR& Position, 
	_In_ int TeamID
)
/*++

Routine Description:

	This function processes a minion object.

Arguments:

	Object - The minion object.

	Position - The location of the object in world coordinates (x, y, z).

	TeamID - The object's team.

Return Value:

	None.

--*/
{
	//
	// Is this object a ward?
	//
	bool bIsWard;
	SdkIsMinionWard(Object, &bIsWard);
	if (bIsWard)
	{
		//
		// Should we draw it?
		//
		if (!_g_Minions.bDrawWardVisionRange)
			return;

		if (TeamID != (TEAM_TYPE_NEUTRAL - _g_LocalPlayer.TeamID))
			return;

		//
		// Use the vision range as the radius of the circle.
		//
		float VisionRange;
		SdkGetMinionVisionRadius(Object, &VisionRange);

		float BRadius;
		SdkGetObjectBoundingRadius(Object, &BRadius);

		VisionRange += BRadius;

		//
		// Draw the circle.
		//
		SdkDrawCircle(&Position, VisionRange, &_g_ColorPurple, 0, &_g_DirectionVector);

		//
		// Get the unique network ID of the object.
		//
		unsigned int NetworkID;
		SdkGetObjectNetworkID(Object, &NetworkID);

		//
		// Should we ping this object when we first see it?
		//
		if (_g_Minions.bPingWards && !_g_Minions.PingedWards[NetworkID])
		{
			//
			// Avoid re-pinging the object each frame.
			//
			_g_Minions.PingedWards[NetworkID] = true;

			//
			// Send a ping to the minimap.
			//
			SdkPingMap(&Position, PING_TYPE_WARDED, true);
		}

		//
		// Get the time remaining for the ward, if necessary.
		// Wards have their expiry time in an ability resource.
		//
		SDK_ABILITY_RESOURCE AbilityResources;
		SdkGetUnitAbilityResource(Object, ABILITY_SLOT_PRIMARY, &AbilityResources);

		//
		// If this ward has remaining time left, render it.
		//
		if (AbilityResources.Current >= 0.01f)
		{
			std::stringstream ss;
			ss.precision(0);
			ss.setf(std::ios_base::fixed, std::ios_base::floatfield);

			ss << "Remaining: " << AbilityResources.Current << "\n";

			//
			// Place the text above the ward.
			//
			SdkDrawText(&Position, NULL, ss.str().c_str(), "Arial Narrow", &_g_ColorYellow, 14, 0, 0, false);
		}

		//
		// Get the minimap coordinates for this ward.
		//
		SDKPOINT MinimapScreen;
		SdkWorldToMinimap(&Position, &MinimapScreen);

		//
		// Place text at minimap location showcasing that there is a 
		// ward in the area.
		//
		SdkDrawText(NULL, &MinimapScreen, "W", "Arial Narrow", &_g_ColorYellow, 8, 0, 0, false);

	}
	//
	// Should we draw eligible last-hit targets?
	//
	else if (_g_Minions.bDrawLastHittable && TeamID != _g_LocalPlayer.TeamID)
	{
		//
		// Get the health of the unit.
		//
		SDK_HEALTH Health;
		SdkGetUnitHealth(Object, &Health);

		//
		// Get the player's current attack damage.
		//
		float AttackDamage;
		SdkGetAIAttackDamage(_g_LocalPlayer.Object, &AttackDamage);

		//
		// Will this attack kill the minion (health + shields)?
		//
		if ((Health.Current + Health.PhysicalShield) > AttackDamage)
			return;

		//
		// Highlight the minion as an eligible last-hit target.
		//
		SdkDrawCircle(&Position, 100.f, &_g_ColorWhite, 0, &_g_DirectionVector);
	}
}

static
void 
ProcessTurret(
	_In_ void* Object, 
	_In_ SDKVECTOR& Position, 
	_In_ int TeamID
)
/*++

Routine Description:

	This function processes a turret object.

Arguments:

	Object - The turret object.

	Position - The location of the object in world coordinates (x, y, z).

	TeamID - The object's team.

Return Value:

	None.

--*/
{
	//
	// Should we draw the tower's attack range?
	//
	if (!_g_Turrets.bDrawAttackRange)
		return;

	//
	// Is this tower an enemy or an ally turret?
	//
	if (_g_Turrets.bDrawOnlyEnemies && TeamID == _g_LocalPlayer.TeamID)
		return;

	int TurretPosition, TurretLane;
	SdkGetTurretInfo(Object, &TurretPosition, &TurretLane);

	//
	// Attack ranges for turrets. 
	// A turret position of 0 implies the nexus tower.
	//
	float Radius = ((TurretPosition == 0) ? 1400.f : 875.f);

	if (TeamID == TEAM_TYPE_CHAOS && TurretPosition == 0)
	{
		//
		// Visibility fix for red turret.
		//
		Position.x += 50.f;
		Position.y -= 300.f;
	}

	//
	// Draw the circle.
	//
	SdkDrawCircle(&Position, Radius, ((TeamID == _g_LocalPlayer.TeamID) ? &_g_ColorGreen : &_g_ColorRed), 0, &_g_DirectionVector);
}

static
void 
ProcessHero(
	_In_ void* Object,
	_In_ SDKVECTOR& Position,
	_In_ int TeamID
)
/*++

Routine Description:

	This function processes a hero object.

Arguments:

	Object - The hero object.

	Position - The location of the object in world coordinates (x, y, z).

	TeamID - The object's team.

Return Value:

	None.

--*/
{
	UNREFERENCED_PARAMETER(TeamID);

	//
	// Should we track spell cooldowns?
	//
	if (!_g_Heroes.bTrackCooldowns)
		return;

	//
	// Should we include the local player?
	//
	if (!_g_Heroes.bIncludeLocalPlayer && Object == _g_LocalPlayer.Object)
		return;

	//
	// Get the current game time.
	//
	float Time;
	SdkGetGameTime(&Time);

	std::stringstream ss;
	ss.precision(0);
	ss.setf(std::ios_base::fixed, std::ios_base::floatfield);

	//
	// Should we include summoner spells?
	//
	uint8_t Spells = ((_g_Heroes.bIncludeSummonerSpells) ? RTL_NUMBER_OF(_g_Spells) : RTL_NUMBER_OF(_g_Spells) - 2);

	for (uint8_t i = 0; i < Spells; ++i)
	{
		//
		// Get the spell of interest.
		//
		SDK_SPELL Spell;
		SdkGetAISpell(Object, i, &Spell);

		//
		// Don't care about spells that haven't been learned yet.
		//
		if (!Spell.Learned)
			continue;

		//
		// Calculate when the spell will be off cooldown.
		//
		float Delta = ((Time < Spell.CooldownExpires) ? (Spell.CooldownExpires - Time) : 0);

		//
		// Display info about this spell.
		//
		if (Delta >= 0.01f || _g_Heroes.bIncludeSpellsOffCooldown)
			ss << _g_Spells[i] << ": " << Delta << "\n";

	}

	//
	// Place the spell cooldown state above the champion.
	//
	SdkDrawText(&Position, NULL, ss.str().c_str(), "Arial Narrow", &_g_ColorYellow, 14, 0, 0, false);
}

bool 
__cdecl 
AwObjectLoop(
	_In_ void* Object, 
	_In_ unsigned int NetworkID,
	_In_opt_ void* UserData
)
/*++

Routine Description:

	This function is invoked for each game object in the frame.

Arguments:
	
	Object - The game object.

	NetworkID - The unique network ID of the object.

	UserData - A pointer to an arbitrary data structure, provided
		by the user, when the overlay scene was registered.

Return Value:

	To stop iterating game objects, return false. To continue iterating
	over game objects, return true.

--*/
{
	UNREFERENCED_PARAMETER(NetworkID);
	UNREFERENCED_PARAMETER(UserData);

	//
	// Is this object dead?
	//
	bool bDead;
	SdkIsObjectDead(Object, &bDead);
	if (bDead)
		return true;

	//
	// We're not interested in non-attackable units.
	//
	if (!SDKSTATUS_SUCCESS(SdkIsObjectUnit(Object)))
		return true;

	//
	// Another 'is dead' check.
	//
	SDK_HEALTH Health;
	SdkGetUnitHealth(Object, &Health);
	if (Health.Current < 1.0f)
		return true;

	//
	// Can this object be seen in the fog of war?
	//
	bool bInFogOfWar;
	SdkIsUnitVisible(Object, &bInFogOfWar);
	if (!bInFogOfWar)
		return true;

	//
	// Get the position for the object.
	//
	SDKVECTOR Position;
	SdkGetObjectPosition(Object, &Position);

	//
	// Get the team for the object.
	//
	int TeamID;
	SdkGetObjectTeamID(Object, &TeamID);

	//
	// Process the local player.
	//
	if (_g_LocalPlayer.Object == Object)
		ProcessLocalPlayer(Position);

	//
	// Process all champions.
	//
	if (SDKSTATUS_SUCCESS(SdkIsObjectHero(Object)))
		ProcessHero(Object, Position, TeamID);

	//
	// Process all towers.
	//
	if (SDKSTATUS_SUCCESS(SdkIsObjectTurret(Object)))
		ProcessTurret(Object, Position, TeamID);

	//
	// Process all minions and wards.
	//
	if (SDKSTATUS_SUCCESS(SdkIsObjectMinion(Object)))
		ProcessMinion(Object, Position, TeamID);

	return true;
}

