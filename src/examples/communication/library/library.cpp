/*++

Module Name:

   library.cpp

Abstract:

	This module showcases how to use the Rift API to perform plugin
	to plugin communication. Specifically, this module is considered
	the 'library' and services requests from other plugins.

--*/

//
// This lets us use Windows types, structures, and defined API.
//
#include <Windows.h>

//
// To use mutable string objects.
//
#include <string>

//
// C++-specific containers.
//
#include <vector>

//
// In order to use the Rift API, we include this.
//
#include "../../../sdkapi.h"

//
// Each library should have its own API header as well. All plugins
// that wish to communicate with your library should include this 
// header.
//
#include "../libapi.h"

//
// This stores a pointer to all the SDK functions we can use. This is 
// intentionally a global so that other functions can use it.
//
PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

//
// This is a simple class (structure) that is shared between the library
// and its dependents. 
//
ITestInterface _g_TestInterface;

//
// A message response from the library, when the appropriate imported 
// function is invoked by the requestor.
//
const char _g_MessageResponse[] = "Hello, world... from the library!";

//
// Performs a quick validity check against the requesting plugins 
// input/output buffers.
//
#define BAD_BUFFER_CHECK(Buffer, Actual, Expected) \
	if (Actual < Expected) \
	{ \
		SdkUiConsoleWrite("[library] Bad buffer size: need at least %zu bytes, but got only %zu bytes instead.\n", (size_t)Expected, (size_t)Actual); \
		return false; \
	} \
	if (IsBadWritePtr(Buffer, Actual)) \
	{ \
		SdkUiConsoleWrite("[library] Bad buffer: not writable (0x%p, %zu bytes).\n", Buffer, Actual); \
		return false; \
	}


bool 
__cdecl 
LibraryCallback(
	_In_ const char* ImportName, 
	_In_ unsigned int Version, 
	_Inout_ void* Data, 
	_Inout_ size_t* SizeOfData, 
	_In_ void* UserData
)
/*++

Routine Description:

	This function is called each time a plugin requests to communicate
	with your module.

Arguments:

	ImportName - The function or data within your plugin that is being 
		requested.

	Version - The version of data that the requesting module is
		interested in retrieving. This should probably be a macro 
		similar to SDK_VERSION. Its goal should be to ensure that your 
		library, which is loaded in memory, is compatible with the 
		version of the library that the requesting module was built with.

	Data - On success, the library stores the data that was requested 
		by the caller in this buffer.

	SizeOfData - On input, this value is read to determine how much 
		memory is allocated for the data buffer. On output, your 
		library should adjust this value to the actual data stored
		in the buffer.

	UserData - A pointer to an arbitrary data structure, provided by 
		the user during callback registration (SdkRegisterLibrary).

Return Value:

	If your library successfully handled this request, return TRUE.
	If your library did not successfully handle the incoming request,
	return FALSE.

--*/
{
	//
	// This parameter is unused.
	//
	UNREFERENCED_PARAMETER(UserData);

	//
	// Ensure that your library, which is loaded in memory, is 
	// compatible with the version of the library that the 
	// requesting module was built with.
	//
	if (Version != LIBRARY_VERSION)
	{
		SdkUiConsoleWrite("[library] Requestor was built with an incompatible version of libapi.h. Version must be: %u (but got %u).\n", LIBRARY_VERSION, Version);
		return false;
	}

	//
	// Validate the requestor's inputs.
	//
	// IsBadWritePtr checks that the SizeOfData memory location is 
	// writable (4 bytes).
	//
	if (!Data || !SizeOfData || IsBadWritePtr(SizeOfData, sizeof(SizeOfData)))
	{
		SdkUiConsoleWrite("[library] Requestor provided bad arguments.\n");
		return false;
	}

	//
	// Add some useful debugging information.
	//
	SdkUiConsoleWrite("[library] Requestor (version: %u) wants data for import '%s'.\n", Version, ImportName);

	//
	// Handle the HELLO import request.
	//
	if (!strcmp(ImportName, LIBRARY_IMPORT_HELLO))
	{
		BAD_BUFFER_CHECK(Data, *SizeOfData, sizeof(_g_MessageResponse));

		//
		// Store off the welcome message.
		//
		char* Message = (char*)Data;
		strcpy_s(Message, *SizeOfData, _g_MessageResponse);
		*SizeOfData = sizeof(_g_MessageResponse);
	}
	//
	// Handle the TESTINTERFACE import request.
	//
	else if (!strcmp(ImportName, LIBRARY_IMPORT_TESTINTERFACE))
	{
		BAD_BUFFER_CHECK(Data, *SizeOfData, sizeof(ITestInterface*));

		//
		// Send the pointer of the ITestInterface class to the requestor.
		// 
		// When passing pointers, be cautious. It's important that your 
		// library doesn't free memory that a requesting plugin assumes
		// is valid. Likewise, it's bad practice for a requesting plugin
		// to free memory that is under your library's control.
		//
		// Instead of using raw pointers, a more elegant solution is to
		// use std::shared_ptr (which contains reference counting and 
		// automatic garbage collection).
		//
		ITestInterface** TestInterface = (ITestInterface**)Data;
		*TestInterface = &_g_TestInterface;
		*SizeOfData = sizeof(*TestInterface);
	}
	//
	// Handle the STLCONTAINER import request.
	//
	else if (!strcmp(ImportName, LIBRARY_IMPORT_STLCONTAINER))
	{
		BAD_BUFFER_CHECK(Data, *SizeOfData, sizeof(std::vector<std::string>));

		//
		// Fill up the requestor's std::vector.
		//
		std::vector<std::string>* Strings = (std::vector<std::string>*)Data;

		for (size_t i = 0; i < 10; ++i)
		{
			//
			// It's not considered good practice for a library to do 
			// memory allocations on behalf of a requestor unless the 
			// library will also free those allocations (which, in this 
			// case, it won't).
			//
			Strings->push_back("Hello, " + std::to_string(i) + ".");
		}

		*SizeOfData = sizeof(*Strings);
	}
	else
	{
		//
		// Unknown request, log it and return false.
		//

		SdkUiConsoleWrite("[library] Requestor wants import '%s', but it doesn't exist.\n", ImportName);

		return false;
	}

	//
	// All's good!
	//

	return true;
}

BOOL
WINAPI
DllMain(
	_In_ HINSTANCE hinstDLL,
	_In_ DWORD fdwReason,
	_In_ LPVOID lpvReserved)
/*++

Routine Description:

	This function is called when the SDK loads the module. Consider
	this your module entry point.

Arguments:

	hinstDLL - The same value as lpvReserved when loaded by the SDK.

	fdwReason - Exclusively DLL_PROCESS_ATTACH (1) when loaded by the
		SDK.

	lpvReserved - The same value as hinstDLL when loaded by the SDK.
		Contains a pointer to the SDK_CONTEXT structure that can be
		retrieved via the SDK_EXTRACT_CONTEXT macro. This structure
		contains all the necessary function pointers required to
		interface with the Rift SDK.

Return Value:

	'TRUE' indicates module load success. 'FALSE' indicates module
	load failure.

--*/
{
	//
	// This parameter is unused.
	//
	UNREFERENCED_PARAMETER(hinstDLL);

	//
	// We're only interested when the DLL attaches to the process.
	//
	if (fdwReason != DLL_PROCESS_ATTACH)
		return TRUE;

	//
	// This macro extracts the pointer to the SDK context from the lpvReserved
	// parameter.
	//
	SDK_EXTRACT_CONTEXT(lpvReserved);
	if (!SDK_CONTEXT_GLOBAL)
		return FALSE;

	//
	// Every module loaded by Rift should have a call to SdkNotifyLoadedModule
	// as soon as possible to ensure that other SDK API can be called.
	//
	if (!SDKSTATUS_SUCCESS(SdkNotifyLoadedModule(LIBRARY_NAME, SDK_VERSION)))
	{
		//
		// This routine will fail if a module of the same name is already
		// loaded or if there's a mismatch with the target SDK version.
		//
		return FALSE;
	}

	//
	// In order to use basically any SDK function (aside from 
	// SdkNotifyLoadedModule and SdkRegisterOnLoad), we need to be
	// in the main game thread. 
	//
	// All callbacks are executed in the context of the main game 
	// thread. Calling SdkRegisterOnLoad here ensures that we can 
	// use other SDK API safely as it will execute in the main
	// game thread.
	//
	SdkRegisterOnLoad
	(
		[](void* UserData) -> void
		{
			UNREFERENCED_PARAMETER(UserData);

			//
			// The initial magic value of ITestInterface::_Value.
			//
			_g_TestInterface.SetValue(0xBEEFD00D);

			//
			// Register a callback to service API requests for this library.
			//
			SdkRegisterLibrary(LibraryCallback, NULL);
		}, NULL
	);

	return TRUE;
}

