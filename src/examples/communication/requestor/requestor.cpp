/*++

Module Name:

   requestor.cpp

Abstract:

	This module showcases how to use the Rift API to perform plugin
	to plugin communication. Specifically, this module is considered
	a 'requestor' and asks for functions from the 'library' plugin.

	NOTE: If your module DEPENDS on another module, the dependent module 
	must be loaded before yours, otherwise, SdkGetLibraryImport will
	fail with status code SDKSTATUS_LIBRARY_NOT_LOADED. Simply put,
	the 'Library' module must be loaded before the 'Requestor'
	module in our case. 
	
	For development builds, this can be done by first issuing a
	LOADMODULE command to library.dll followed by a LOADMODULE command
	to requestor.dll (this plugin).

	For production builds, dependencies can be managed through the
	portal in the 'Plugins' (developer-only) tab. There is a section
	called 'Plugin Dependencies' that you can use for this purpose.
	Simply add the 'plugin code' that your plugin depends on and the
	required library will be automatically loaded before yours by the 
	SDK. A 'plugin code' can be seen in the URL immediately following
	the `?code=` token.

	When a plugin is unloaded through the UNLOADMODULE command and there 
	are other active plugins that still depend on it, all dependent 
	plugins will be unloaded first.

--*/

//
// This lets us use Windows types, structures, and defined API.
//
#include <Windows.h>

//
// To use mutable string objects.
//
#include <string>

//
// C++-specific containers.
//
#include <vector>

//
// In order to use the Rift API, we include this.
//
#include "../../../sdkapi.h"

//
// Each library should have its own API header as well. All plugins
// that wish to communicate with a certain library should include the
// appropriate header.
//
#include "../libapi.h"

//
// Stringification macros.
//
#define STR(s) #s
#define XSTR(s) STR(s)

//
// Runs a test case on an import from the library.
//
#define MAKE_TEST(ImportName, Conditional, Testcase, OnSuccess, OnFailure) \
	Status = SdkGetLibraryImport(LIBRARY_NAME, ImportName, LIBRARY_VERSION, Data, &SizeOfData); \
	SdkUiConsoleWrite("[requestor] Test " XSTR(__COUNTER__) ": " Testcase " Status: 0x%x [%s].", Status, ((Conditional) ? "success" : "error")); \
	if (Conditional) \
	{ \
		OnSuccess; \
	} \
	else \
	{ \
		OnFailure; \
		return; \
	}

//
// This stores a pointer to all the SDK functions we can use. This is 
// intentionally a global so that other functions can use it.
//
PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

BOOL
WINAPI
DllMain(
	_In_ HINSTANCE hinstDLL,
	_In_ DWORD fdwReason,
	_In_ LPVOID lpvReserved)
/*++

Routine Description:

	This function is called when the SDK loads the module. Consider
	this your module entry point.

Arguments:

	hinstDLL - The same value as lpvReserved when loaded by the SDK.

	fdwReason - Exclusively DLL_PROCESS_ATTACH (1) when loaded by the
		SDK.

	lpvReserved - The same value as hinstDLL when loaded by the SDK.
		Contains a pointer to the SDK_CONTEXT structure that can be
		retrieved via the SDK_EXTRACT_CONTEXT macro. This structure
		contains all the necessary function pointers required to
		interface with the Rift SDK.

Return Value:

	'TRUE' indicates module load success. 'FALSE' indicates module
	load failure.

--*/
{
	//
	// This parameter is unused.
	//
	UNREFERENCED_PARAMETER(hinstDLL);

	//
	// We're only interested when the DLL attaches to the process.
	//
	if (fdwReason != DLL_PROCESS_ATTACH)
		return TRUE;

	//
	// This macro extracts the pointer to the SDK context from the lpvReserved
	// parameter.
	//
	SDK_EXTRACT_CONTEXT(lpvReserved);
	if (!SDK_CONTEXT_GLOBAL)
		return FALSE;

	//
	// Every module loaded by Rift should have a call to SdkNotifyLoadedModule
	// as soon as possible to ensure that other SDK API can be called.
	//
	if (!SDKSTATUS_SUCCESS(SdkNotifyLoadedModule("Requestor", SDK_VERSION)))
	{
		//
		// This routine will fail if a module of the same name is already
		// loaded or if there's a mismatch with the target SDK version.
		//
		return FALSE;
	}

	//
	// In order to use basically any SDK function (aside from 
	// SdkNotifyLoadedModule and SdkRegisterOnLoad), we need to be
	// in the main game thread. 
	//
	// All callbacks are executed in the context of the main game 
	// thread. Calling SdkRegisterOnLoad here ensures that we can 
	// use other SDK API safely as it will execute in the main
	// game thread.
	//
	SdkRegisterOnLoad
	(
		[](void* UserData) -> void
		{
			UNREFERENCED_PARAMETER(UserData);

			//
			// Setup test buffers (MAKE_TEST uses these to pass in input to 
			// the library).
			//
			size_t SizeOfData = 0;
			void* Data = NULL;
			SDKSTATUS Status = SDKSTATUS_NO_ERROR;

			//
			// Call LIBRARY_IMPORT_HELLO, but with bad input.
			//
			MAKE_TEST
			(
				LIBRARY_IMPORT_HELLO,
				Status == SDKSTATUS_LIBRARY_IMPORT_NOT_RESOLVED,
				"Zero sized data buffer (testing failure).",
				__noop,
				__noop
			);


			//
			// Call LIBRARY_IMPORT_HELLO, but with good input this time.
			//
			char Message[100] = { 0 };
			SizeOfData = sizeof(Message);
			Data = &Message[0];

			MAKE_TEST
			(
				LIBRARY_IMPORT_HELLO,
				SDKSTATUS_SUCCESS(Status),
				"Message retrieval.",
				SdkUiConsoleWrite("[requestor] Message received from library: %s\n", Message); ,
				__noop
			);

			//
			// Call LIBRARY_IMPORT_TESTINTERFACE, get the ITestInterface object,
			// retrieve its value, and change it to a new value.
			//
			ITestInterface* TestInterface = NULL;
			SizeOfData = sizeof(TestInterface);
			Data = &TestInterface;
			MAKE_TEST
			(
				LIBRARY_IMPORT_TESTINTERFACE,
				SDKSTATUS_SUCCESS(Status),
				"Passing an object around... 1 / 2",
				SdkUiConsoleWrite("[requestor] The value of the object is: 0x%X.\n", TestInterface->GetValue());
				TestInterface->SetValue(0xDEADB00B); ,
				__noop
				);

			//
			// Call LIBRARY_IMPORT_TESTINTERFACE once more, get the ITestInterface 
			// object, and retrieve its value. It should have updated to the value
			// that was set in the previous call.
			//
			MAKE_TEST
			(
				LIBRARY_IMPORT_TESTINTERFACE,
				SDKSTATUS_SUCCESS(Status),
				"Passing an object around... 2 / 2",
				SdkUiConsoleWrite("[requestor] The value of the object is: 0x%X.\n", TestInterface->GetValue()); ,
				__noop
			);

			//
			// Call LIBRARY_IMPORT_STLCONTAINER to fill up an std::vector with 
			// std::strings from the library and display them.
			//
			std::vector<std::string> Strings;
			SizeOfData = sizeof(Strings);
			Data = &Strings;

			MAKE_TEST
			(
				LIBRARY_IMPORT_STLCONTAINER,
				SDKSTATUS_SUCCESS(Status),
				"Passing an STL container...",
				SdkUiConsoleWrite("[requestor] Contents of vector (%zu): \n", Strings.size());
				for (auto& s : Strings)
				{
					SdkUiConsoleWrite("\t- %s\n", s.c_str());
				},
				__noop
				);
		}, NULL
	);

	return TRUE;
}