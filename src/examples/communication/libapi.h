/*++

Module Name:

   libapi.h

Abstract:

	Exposes the library API to developers.

--*/

#pragma once

//
// The name of the library that implements these "useful" functions.
//
#define LIBRARY_NAME "Library"

//
// The version of the libapi header that the module was built with. 
// This value is passed to the SdkGetLibraryImport function to
// ensure that versions are compatible at runtime.
//
#define LIBRARY_VERSION 1

//
// The imported functions that this library exposes for calling 
// modules to request.
//
#define LIBRARY_IMPORT_HELLO "HELLO"
#define LIBRARY_IMPORT_TESTINTERFACE "TESTINTERFACE"
#define LIBRARY_IMPORT_STLCONTAINER "STLCONTAINER"

//
// A simple class that is passed from the library to the calling
// module when requested (LIBRARY_IMPORT_TESTINTERFACE).
//
class ITestInterface
{
private:
	int _Value;

public:

	int GetValue()
	{
		return _Value;
	}

	void SetValue(int Value)
	{
		_Value = Value;
	}
};