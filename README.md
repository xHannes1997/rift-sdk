# Rift Development Kit

## Welcome

This documentation is meant to be a guide on creating plugins for Rift. Please read through it before starting your first project. If you have a problem or need clarification, [create an issue](https://gitlab.com/riftlol/rift-sdk/issues) and we will answer your question and improve the documentation if necessary. You will need a Rift subscription from [our site](https://rift.lol) in order to develop, and when you are ready to post and sell your creations in our marketplace, [visit our forums](https://rift.lol/community/) and reach out to us.

## Contents

[Getting Started](#getting-started)
* [Requirements](#requirements)
* [Cloning](#cloning)

[Building a Plugin](#building-a-plugin)
* [Creating Your Project](#creating-your-project)
* [Include Header](#include-header)
* [Basic Code](#basic-code)
* [Compiling](#compiling)

[Important Concepts](#important-concepts)
* [Game Object Hierarchy](#game-object-hierarchy)

[Testing a Plugin](#testing-a-plugin)
* [Using Crash Dumps](#using-crash-dumps)

[FAQ](#faq)

## Getting Started

### Requirements

In order to build Rift plugins, you will need the following:
* [Visual Studio 2017](https://visualstudio.microsoft.com/vs/)
* [Windows SDK](https://developer.microsoft.com/en-us/windows/downloads/windows-10-sdk)
* [League of Legends](https://euw.leagueoflegends.com/en/)

Because of the security features we use in Rift to keep our community safe, you will _not_ be able to test your plugins in a virtual machine. Please use physical hardware for your development process.

### Cloning

Before starting your first plugin, you should browse through the API and sample code. The best way to do this is to clone the entire SDK repository locally using your favorite Git shell, or by running the command-line operation below:

```git clone https://gitlab.com/riftlol/rift-sdk.git```

## Building a Plugin

### Creating Your Project

To get started with your own plugin you need to create a new project in Visual Studio. You can do this by going to _File -> New -> Project_ and then selecting _Visual C++ -> Windows Desktop -> Dynamic-Link Library_. 

It's also important that you statically link the CRT with your project. That means that the "Runtime Library" must be "Multi-Threaded /MT" for release configurations and "Multi-Threaded Debug /MTd" for debug configurations. You can do this by right-clicking on your project in the "Solution Explorer", selecting "C/C++" from the configuration properties pane, and clicking on "Code Generation". Adjust the "Runtime Library" field appropriately based on build configuration:

![static linking](https://i.imgur.com/5gdJfNT.png)

Or if you want to save some steps, you can make a copy of the template project that we've included in the repository. This will get you all set up with the correct project structure and some boilerplate code.

### Include Header

You need to include the header file `sdkapi.h` in your project, and then include it in your source file:

```
#include <sdkapi.h>
```

### Basic Code

Your plugin needs some simple plumbing in order for it to work correctly. Once you have included the header, you need to write some boilerplate code to get things going. If you're using the sample project, this is done for you.

First a new global needs to be defined to represent the SDK context:

```
PSDK_CONTEXT SDK_CONTEXT_GLOBAL;
```

Then in your `DllMain` handler you need to extract the SDK context:

```
	//
	// We're only interested when the DLL attaches to the process.
	//
	if (fdwReason != DLL_PROCESS_ATTACH)
		return TRUE;

	//
	// This macro extracts the pointer to the SDK context from the lpvReserved
	// parameter.
	//
	SDK_EXTRACT_CONTEXT(lpvReserved);
	if (!SDK_CONTEXT_GLOBAL)
		return FALSE;

	//
	// Every module loaded by Rift should have a call to SdkNotifyLoadedModule
	// as soon as possible to ensure that other SDK API can be called.
	//
	if (!SDKSTATUS_SUCCESS(SdkNotifyLoadedModule("Simple", SDK_VERSION)))
	{
		//
		// This routine will fail if a module of the same name is already
		// loaded or if there's a mismatch with the target SDK version.
		//
		return FALSE;
	}

```

If that was successful, you can begin the logic of your plugin. With our sample plugin we just print a message to the console saying hello, but you could do more complex initialization here.

```
	//
	// Write a "hello world" message to the developer console. 
	//
	// We use the Windows API (GetCurrentProcessId and GetCurrentThreadId) to show 
	// that it's easy and possible to do!
	//
	SdkUiConsoleWrite("[simple] Hello! Executing in process %u and thread %u.\n", GetCurrentProcessId(), GetCurrentThreadId());

	return TRUE;


```

### Compiling

When you're satisfied with your plugin you can compile it as a 32-bit library. Make sure that _x86_ is selected from the platform dropdown.

## Important Concepts

### Game Object Hierarchy

Game objects in League of Legends deeply inherit from each other. We've provided some sample code that shows your how to iterate through the game objects on the map. The most basic type of object is `GameObject`. The others, and what they inherit from, are listed below:

|Class|Inherits From|Properties|
|---|---|---|
|`GameObject`||Basic near-abstract game object|
|`AttackableUnit`|`GameObject`|Targetable unit that has health and can take damage|
|`AIBaseClient`|`GameObject`->`AttackableUnit`|Basic computer-controlled unit|
|`AIHeroClient`|`GameObject`->`AttackableUnit`->`AIBaseClient`|Hero that can be human or computer-controlled|
|`AIMinionClient`|`GameObject`->`AttackableUnit`->`AIBaseClient`|Minion that is computer-controlled|
|`AITurretClient`|`GameObject`->`AttackableUnit`->`AIBaseClient`|Turret that is computer-controlled|

## Testing a Plugin

Once you have built your plugin you can test it locally without having to upload it to the web portal. Just start a game of League of Legends and run the Rift client (or in the opposite order if you choose, it works either way). Once you're in-game, press _F11_ to bring up the console and use the following command:

```LOADMODULE <path>```

For example:

```LOADMODULE C:\spaces are allowed\myplugin.dll```

You can unload a plugin (if you want to rebuild it) by referencing it by name:

```UNLOADMODULE myplugin```

Here are some other helpful console commands:
* `HELP` lists all commands.
* `HISTORY` shows the history of commands entered.
* `CLEAR` will clear the console.
* `EXIT` will exit the console.
* `MODULES` will show a list of loaded plugins.

### Using Crash Dumps

If your plugin experiences an error during the game the Rift SDK may generate a dump file for you. You can use this dump file to diagnose the code that caused the problem. To open this dump file, you'll need to [install WinDbg](https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/) and follow these instructions:

1. Open the _x86_ version of WinDbg. 
2. Go to _File -> Open Crash Dump_ and open the dump file that was generated for you. Rift creates a file in your League of Legends folder with the `RFT` prefix (and ending with `.tmp`).
3. A console window with some output and a command line will appear. Before proceeding further make sure the DLL and PDB of the exact version of the plugin that crashed are also in the same directory as the dump file.
4. In the command line type `.sympath+ "{PATH}"` where `{PATH}` is the path containing your project's PDB file. For example, if your plugin's PDB file was in _C:\Projects\Plugins\MyPlugin\bin\Debug_, you would type the command `.sympath+ "C:\Projects\Plugins\MyPlugin\bin\Debug"` and press enter.
5. The console window will also contain a string containing the base address of your plugin in the dump. Look for the string `Comment:` near the top of the output, followed by the loaded plugins, to find out where your plugin is located in memory.
6. Using the address from the previous step, you can force WinDbg to use your PDB at the target address. Type `.reload /i {PLUGIN}={ADDRESS}` and press enter. For example, if the plugin's address was _0x03AF0000_ and the name was _MyPlugin.dll_, the command would be `.reload /i MyPlugin.dll=0x03AF0000`.
7. You can now examine the exception record by typing `.ecxr` and pressing enter. Using the command `.exr -1` will show you the type of exception that occurred.
8. You can type `kn` and press enter to view the call stack. You can also select _View -> Locals_ to see local variables.

## FAQ

#### Do I need to do anything to avoid detection?

If you use Rift-provided APIs your plugin will be automatically protected. Using external APIs can make detection easier in some cases. If you are unsure whether a particular API will make you easier to detect don't hesitate to contact us.

#### Will you change the SDK functions?

We will try our best to maintain backwards compatibility unless there are significant engine changes so that you do not have to change your plugin code.

#### Can I get new features added to the SDK?

Yes! We welcome suggestions from our developers and are happy to add new features if you need them.